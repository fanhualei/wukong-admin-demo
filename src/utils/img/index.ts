/**
 * 得到一个Base64的文件内容
 * 使用方法：file.preview = await getBase64(file.originFileObj);
 * @param file
 * @returns
 */
export function getBase64(file: Blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
