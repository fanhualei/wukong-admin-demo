/**
 * 参考网址
 * https://dayjs.gitee.io/docs/zh-CN/manipulate/add#list-of-all-available-units
 */

import dayjs from 'dayjs';

/**
 * 得到当前的开始时间
 * return: 例如 2021-04-27
 */
export function startOfToday() {
  return dayjs().startOf('date').format('YYYY-MM-DD');
}

/**
 * 2021-04-12,2021-04-27
 * @param beforeDay 今天之前的日期
 */

/**
 * 给定一个时间区间，返回之前多少天
 * @param addDay  例如：15,今天之后的15天
 * @returns 一个用逗号分割的字符串，例如：2021-04-12,2021-04-27
 */
export function getDistanceFromToday(addDay: number) {
  const startDay = dayjs().add(addDay, 'day').format('YYYY-MM-DD');
  return `${startOfToday()},${startDay}`;
}

export function formatDate(dateStr: string | undefined) {
  if (!dateStr) {
    return '';
  }
  return dayjs(dateStr).format('YYYY-MM-DD');
}
