/**
 * 字符串工具类
 */

/**
 * 首字母转换成大写
 * @param str  abcdEf => AbcdEf
 * StringUtils.capitalize("cat") = "Cat"
 * StringUtils.capitalize("cAt") = "CAt"
 * @returns
 */
export function capitalize(str: string) {
  if (!str) {
    return undefined;
  }
  let ren: string = str;
  ren = str.substring(0, 1).toUpperCase().concat(str.substring(1, str.length));
  return ren;
}

/**
 * 将首字母转换成小写
 * @param str 
 *  StringUtils.uncapitalize("cat") = "cat"
 * StringUtils.uncapitalize("Cat") = "cat"
 * StringUtils.uncapitalize("CAT") = "cAT"
   
 * @returns 
 */
export function uncapitalize(str: string) {
  if (!str) {
    return undefined;
  }
  let ren: string = str.toString();
  ren = ren.substring(0, 1).toLowerCase().concat(ren.substring(1, ren.length));
  return ren;
}

/**
 * 为了显示注释，删除数据库Table名备注的最后一个“表”字
 * @param str
 * @returns
 */
export function removeLastWord(str: string) {
  if (!str) {
    return undefined;
  }
  if (str.substring(str.length - 1, str.length) === '表') {
    return str.substring(0, str.length - 1);
  }
  return str;
}
