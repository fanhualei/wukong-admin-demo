import React from 'react';
import { Checkbox, Row, Col } from 'antd';
import type { CheckboxChangeEvent } from 'antd/lib/checkbox';
import type { CheckboxValueType } from 'antd/lib/checkbox/Group';
import type { AdminGroupItem } from '@/services/system/admin';

type CheckGroupListProps = {
  rootPath: string;
  rootCName: string;
  options: AdminGroupItem[];
  defaultChecked: AdminGroupItem[];
  callBack: (cname: string, rootPath: string, checked: AdminGroupItem[]) => void;
};

/**
 * 得到一个pan的数组，例如['/path/1','/path/2']
 * @param checkeds
 */
const getCheckedList = (checkeds: AdminGroupItem[]): CheckboxValueType[] => {
  if (!checkeds || checkeds.length === 0) {
    return [];
  }
  const checkedArray: CheckboxValueType[] = [];
  checkeds.forEach((item) => {
    checkedArray.push(item.path);
  });
  return checkedArray;
};
/**
 * 每一个权限组，都有这么一个组件用来单独显示
 * rootPath：这个权限组的path
 * rootCName: 这个权限组的path
 * callBack: 这个很重要，会调用AdminGroupEdit页面中的函数，用来把数据缓存进去。
 */
const CheckGroupList: React.FC<CheckGroupListProps> = (props: CheckGroupListProps) => {
  const { rootPath, options, defaultChecked, rootCName, callBack } = props;

  // 被选中的数组
  const defaultCheckedList = getCheckedList(defaultChecked);
  const [checkedList, setCheckedList] = React.useState(defaultCheckedList);

  // 是否选中一部分
  const [indeterminate, setIndeterminate] = React.useState(
    !!defaultCheckedList.length && defaultCheckedList.length < options.length,
  );

  // 是否全部选中
  const [checkAll, setCheckAll] = React.useState(defaultCheckedList.length === options.length);

  // 回调父组件的函数
  const handlParentCallback = (checkedListPara: CheckboxValueType[]) => {
    const newCheckList: AdminGroupItem[] = [];
    options.forEach((item) => {
      if (checkedListPara.indexOf(item.path) >= 0) {
        newCheckList.push({
          cname: item.cname,
          path: item.path,
        });
      }
    });
    callBack(rootCName, rootPath, newCheckList);
  };

  /**
   * 全选或全不选按钮被点中
   * @param e  选中或者不选中
   */
  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    const newList: CheckboxValueType[] = e.target.checked ? getCheckedList(options) : [];
    setCheckedList(newList);
    handlParentCallback(newList);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  /**
   * Checkbox.Group 一个组中某个checkbox被改变，就触发这个事件
   * @param list 当前选中的内容
   */
  const onChange = (list: CheckboxValueType[]) => {
    setCheckedList(list);
    handlParentCallback(list);
    setIndeterminate(!!list.length && list.length < options.length);
    setCheckAll(list.length === options.length);
  };

  return (
    <div>
      <Checkbox
        indeterminate={indeterminate}
        onChange={onCheckAllChange}
        checked={checkAll}
        key={rootPath}
        style={{ fontWeight: 600 }}
        disabled={rootCName === '总览'}
      >
        {rootCName}
      </Checkbox>
      <br />
      <Checkbox.Group
        style={{ width: '100%', paddingLeft: 28, marginTop: 10, marginBottom: 10 }}
        value={checkedList}
        onChange={onChange}
        key={`${rootPath}Group`}
      >
        <Row>
          {options.map((item) => (
            <Col span={4} key={`col-${item.path}`}>
              <Checkbox
                value={item.path}
                key={item.path}
                style={{ marginBottom: 10 }}
                disabled={item.path === '/welcome/home'}
              >
                {item.cname}
              </Checkbox>
            </Col>
          ))}
        </Row>
      </Checkbox.Group>
    </div>
  );
};

export default CheckGroupList;
