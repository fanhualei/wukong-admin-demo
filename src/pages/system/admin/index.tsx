import React, { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';

import AdminList from './AdminList';
import AdminGroupList from './AdminGroupList';

export default () => {
  const [tabKey, setTabKey] = useState('admin');
  return (
    <PageContainer
      className="customPageContainer"
      breadcrumb={undefined}
      onTabChange={(key) => {
        setTabKey(key);
      }}
      tabActiveKey={tabKey}
      tabList={[
        {
          tab: '管理员',
          key: 'admin',
        },
        {
          tab: '权限组',
          key: 'role',
        },
      ]}
    >
      {tabKey === 'admin' ? <AdminList /> : <AdminGroupList />}
    </PageContainer>
  );
};
