import React from 'react';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getAdminList, getAdminGroupListOptions, delAdminById } from '@/services/system/admin';
import { Button, Popconfirm, message } from 'antd';
import EnabledText from '@/components/EnabledText';
import type { ActionType } from '@ant-design/pro-table/lib/typing';
import AdminEdit from './AdminEdit';

export default () => {
  const [editVisible, setEditVisible] = React.useState(false);
  const [editId, setEditId] = React.useState('');
  const tableRef = React.useRef<ActionType>();
  // 刷新List
  const listReload = () => {
    tableRef.current?.reload();
  };

  const columns: ProColumns<API.Admin>[] = [
    {
      title: '名称',
      dataIndex: 'adminName',
    },
    {
      title: '上次登录时间',
      dataIndex: 'adminLoginTime',
      valueType: 'dateTime',
    },
    {
      title: '登录次数',
      dataIndex: 'adminLoginNum',
      align: 'right',
    },
    {
      title: '超级管理员',
      dataIndex: 'adminIsSuper',
      render: (_, record) => (record.adminIsSuper ? '是' : '否'),
      align: 'center',
    },
    {
      title: '是否有效',
      dataIndex: 'enabled',
      render: (text) => {
        return <EnabledText enabled={text} />;
      },
      align: 'center',
    },
    {
      title: '权限组',
      dataIndex: 'adminGid',
      request: () => getAdminGroupListOptions([{ value: 0, label: '未指定' }]),
      params: {},
      valueType: 'select',
    },
    {
      title: '操作',
      valueType: 'option',
      render: (_, record) =>
        record.adminIsSuper
          ? [
              <a
                key="editable"
                onClick={() => {
                  setEditId(record.adminId);
                  setEditVisible(true);
                }}
              >
                编辑
              </a>,
            ]
          : [
              <a
                key="editable"
                onClick={() => {
                  setEditId(record.adminId);
                  setEditVisible(true);
                }}
              >
                编辑
              </a>,
              <Popconfirm
                key="popconfirm"
                title="确定要删除吗？"
                onConfirm={() => {
                  delAdminById(record.adminId).then((ren) => {
                    if (ren.success) {
                      message.success('删除成功');
                      listReload();
                    }
                  });
                }}
              >
                <a key="delete">删除</a>
              </Popconfirm>,
            ],
    },
  ];

  return (
    <React.Fragment>
      <ProTable<API.Admin>
        actionRef={tableRef}
        columns={columns}
        rowKey="adminId"
        headerTitle="管理员列表"
        search={false}
        toolBarRender={() => [
          <Button
            key="button"
            type="primary"
            onClick={() => {
              setEditId('0');
              setEditVisible(true);
            }}
          >
            新建
          </Button>,
        ]}
        request={async (params) => {
          const result = await getAdminList({
            current: params.current,
            pageSize: params.pageSize,
          });
          return result.data;
        }}
      />

      {editVisible && (
        <AdminEdit
          visible={editVisible}
          id={editId}
          setVisible={setEditVisible}
          listReload={listReload}
        />
      )}
    </React.Fragment>
  );
};
