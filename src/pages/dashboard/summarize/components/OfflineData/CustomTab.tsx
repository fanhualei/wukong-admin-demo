import React from 'react';
import { Col, Row } from 'antd';
import NumberInfo from '../NumberInfo';
import { RingProgress } from '@ant-design/charts';
import type { OfflineDataType } from '../../service';

type CustomTabProps = {
  data: OfflineDataType;
};

export default (props: CustomTabProps) => {
  const { data } = props;
  const config = {
    height: 50,
    width: 50,
    autoFit: false,
    percent: data.cvr,
    color: ['#58afff', '#E8EDF3'],
    // 控制环的代销
    innerRadius: 0.7,
    radius: 0.98,
    // 不显示中间那部分内容
    statistic: {
      content: false,
    },
  };
  return (
    <Row>
      <Col span={12}>
        <NumberInfo title={data.name} subTitle="转化率" gap={2} total={`${data.cvr * 100}%`} />
      </Col>
      <Col span={12} style={{ paddingTop: 36 }}>
        <RingProgress {...config} />
      </Col>
    </Row>
  );
};
