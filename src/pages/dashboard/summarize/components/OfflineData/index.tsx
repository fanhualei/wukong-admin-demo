import { Card, Tabs } from 'antd';
import React from 'react';
import CustomTab from './CustomTab';
import TimelineChart from './TimelineChart';

import { fakeOfflineData, fakeOfflineChartData } from '../../service';
import type { OfflineDataType } from '../../service';

const { TabPane } = Tabs;

export default () => {
  const [activeKey, setActiveKey] = React.useState('store0');
  const [offlineData, setOfflineData] = React.useState<OfflineDataType[]>([]);

  React.useEffect(() => {
    setOfflineData(fakeOfflineData());
  }, []);

  return (
    <Card>
      <Tabs
        activeKey={activeKey}
        onChange={(key) => {
          setActiveKey(key);
        }}
      >
        {offlineData.map((shop, index) => (
          <TabPane key={`store${index}`} tab={<CustomTab data={shop} />}>
            <TimelineChart data={fakeOfflineChartData()} />
          </TabPane>
        ))}
      </Tabs>
    </Card>
  );
};
