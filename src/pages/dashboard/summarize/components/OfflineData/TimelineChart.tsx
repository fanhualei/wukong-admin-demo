import React from 'react';
import { Line } from '@ant-design/charts';

import type { OfflineChartData } from '../../service';

type TimelineChartProps = {
  data: OfflineChartData[];
};

export default (props: TimelineChartProps) => {
  const { data } = props;
  const config = {
    data,
    // smooth: true,
    xField: 'time',
    yField: 'value',
    // 按照那个字段分类
    seriesField: 'name',
    // x轴属性设置，这里设置24，按照每个小时间隔处理
    xAxis: { tickCount: 24 },
    slider: {
      start: 0.1,
      end: 0.9,
    },
    //  别名
    meta: {
      name: {
        formatter: (v: string) => {
          if (v === 'visit') {
            return '客流量';
          }
          if (v === 'pay') {
            return '支付笔数';
          }
          return v;
        },
      },
    },
  };
  return <Line {...config} />;
};
