import { Card, DatePicker, Tabs } from 'antd';
import styles from './index.less';
import React from 'react';
import SalesChart from './SalesChart';
import type { RangePickerProps } from 'antd/es/date-picker/generatePicker';
import { getTimeDistance } from '@/services/Common';

const { RangePicker } = DatePicker;
const { TabPane } = Tabs;

type RangePickerValue = RangePickerProps<moment.Moment>['value'];
type dateTypeProps = 'today' | 'week' | 'month' | 'year';

/**
 * 扩展区域
 * @returns
 */
const TabBarExtra = () => {
  // 设置时间选择区间
  const [dateType, setDateType] = React.useState<dateTypeProps | undefined>('today');

  // 选中的时间区间
  const [rangePickerValue, setRangePickerValue] = React.useState<RangePickerValue>(
    getTimeDistance('today'),
  );

  // 判断当前日期是否被选中
  const isCurrentDateType = (dateTypePrams: dateTypeProps) => {
    if (dateTypePrams === dateType) {
      return styles.currentDate;
    }
    return '';
  };

  // 日期类型被点击后的事件
  const dateTypeClicked = (dateTypePrams: dateTypeProps) => {
    setRangePickerValue(getTimeDistance(dateTypePrams));
    setDateType(dateTypePrams);
  };

  return (
    <div className={styles.salesExtraWrap}>
      <a className={isCurrentDateType('today')} onClick={() => dateTypeClicked('today')}>
        今日
      </a>
      <a className={isCurrentDateType('week')} onClick={() => dateTypeClicked('week')}>
        本周
      </a>
      <a className={isCurrentDateType('month')} onClick={() => dateTypeClicked('month')}>
        本月
      </a>
      <a className={isCurrentDateType('year')} onClick={() => dateTypeClicked('year')}>
        全年
      </a>
      <RangePicker
        value={rangePickerValue}
        onChange={(values) => {
          setRangePickerValue(values);
          setDateType(undefined);
        }}
        style={{ width: 256 }}
      />
    </div>
  );
};

/**
 * 主界面
 * @returns
 */
const SalesCard = () => {
  return (
    // 这里的bodyStyle，可以将内部的缩进，设置成0
    <Card bordered={false} bodyStyle={{ paddingTop: 0 }}>
      <Tabs tabBarExtraContent={<TabBarExtra />} size="large">
        <TabPane tab="销售额" key="sales">
          <SalesChart type="销售额" />
        </TabPane>
        <TabPane tab="访问量" key="views">
          <SalesChart type="访问量" />
        </TabPane>
      </Tabs>
    </Card>
  );
};

export default SalesCard;
