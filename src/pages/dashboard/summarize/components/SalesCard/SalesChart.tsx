import React from 'react';
import { fakeSalesData, fakeRankingListData } from '../../service';
import { Column } from '@ant-design/charts';
import { Col, Row } from 'antd';
import styles from './index.less';
import accounting from 'accounting';

export default ({ type }: { type: string }) => {
  const data = fakeSalesData();
  const rankingListData = fakeRankingListData();
  const config = {
    data,
    height: 255,
    xField: 'x',
    yField: 'y',
    // 柱状图最大宽度
    maxColumnWidth: 20,
    // 提示框中y的别名：type 分别是销售额或访问量
    meta: {
      y: { alias: type },
    },
    // 根据类型，显示不同的颜色
    color: () => {
      if (type === '访问量') {
        return '#13c2c2';
      }
      return '';
    },
  };
  return (
    <div>
      <Row gutter={40}>
        <Col xl={16} lg={12} md={12} sm={24} xs={24}>
          <div style={{ marginBottom: 15 }}>{type}趋势</div>
          <div>
            <Column {...config} />
          </div>
        </Col>
        <Col xl={8} lg={12} md={12} sm={24} xs={24}>
          <div style={{ marginBottom: 15 }}>门店{type}排名</div>
          <ul className={styles.rankingList}>
            {rankingListData.map((item, i) => (
              <li key={item.title}>
                <span className={`${styles.rankingItemNumber} ${i < 3 ? styles.active : ''}`}>
                  {i + 1}
                </span>
                <span className={styles.rankingItemTitle} title={item.title}>
                  {item.title}
                </span>
                <span className={styles.rankingItemValue}>
                  {accounting.formatMoney(item.total, '¥', 0)}
                </span>
              </li>
            ))}
          </ul>
        </Col>
      </Row>
    </div>
  );
};
