import React from 'react';
import { TinyArea } from '@ant-design/charts';
import Tip from '../Tip';

type TinyAreaSearchProps = {
  searchData: { x: string; y: number }[];
};

const TinyAreaSearch: React.FC<TinyAreaSearchProps> = (props: TinyAreaSearchProps) => {
  const { searchData } = props;
  // 必须返回一个number[]
  const data = searchData.map((item) => item.y);
  const config = {
    height: 60,
    data,
    smooth: true,
    // 指定填充的颜色
    areaStyle: { fill: '#d1e9ff' },
    // 为了好看，将线的颜色变成了与背景颜色一样了。
    line: {
      color: '#1089ff',
      style: {
        lineWidth: 2,
      },
    },
    // 按照自定义一个组建叫Tip,来显示提示信息
    tooltip: {
      customContent: (index: number) => {
        if (!searchData[index]) {
          return '';
        }
        return <Tip title={searchData[index].x} value={searchData[index].y} color="#1089ff" />;
      },
    },
  };
  // @ts-ignore
  return <TinyArea {...config} style={{ marginLeft: -10 }} />;
};

export default TinyAreaSearch;
