import React from 'react';
import { Progress } from '@ant-design/charts';

const TinyProgressActive: React.FC = () => {
  const config = {
    height: 60,
    percent: 0.736,
    // 框的大小
    barWidthRatio: 0.25,
    color: ['#13c2c2', '#E8EDF3'],
    // 这个用来显示目标值，里面还有很多属性需要熟悉
    annotations: [
      {
        type: 'line',
        start: ['80%', '30%'],
        end: ['80%', '70%'],
        top: false,
        style: {
          stroke: '#13c2c2',
          lineWidth: 2,
        },
      },
    ],
  };
  // @ts-ignore
  return <Progress {...config} />;
};

export default TinyProgressActive;
