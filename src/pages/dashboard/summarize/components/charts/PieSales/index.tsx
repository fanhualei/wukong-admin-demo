import React from 'react';
import { Pie } from '@ant-design/charts';
import accounting from 'accounting';

type PieSalesProps = {
  data: { x: string; y: number }[];
};

const PieSales: React.FC<PieSalesProps> = (props: PieSalesProps) => {
  const { data } = props;

  const getY = (x: string) => {
    for (let i = 0; i < data.length; i += 1) {
      if (data[i].x === x) {
        return data[i].y;
      }
    }
    return 0;
  };

  const config = {
    appendPadding: 10,
    data,
    height: 400 - 50,
    // 弧度对应的字段
    angleField: 'y',
    // 颜色对应的字段
    colorField: 'x',
    // 饼图的半径，原点为画布中心。配置值域为 (0,1]，1 代表饼图撑满绘图区域。
    radius: 0.68,
    // 饼图的内半径，原点为画布中心。配置值域为 (0,1] ,通过设置这个来做镂空图形
    innerRadius: 0.8,
    // 全局化配置图表数据元信息，以字段为单位进行配置
    meta: {
      x: {
        formatter: (v: string) => {
          return `${v}  ${accounting.formatMoney(getY(v), '¥', 0)}`;
        },
      },
      // 将金额设定成Money的格式
      y: {
        formatter: (v: number) => {
          return `${accounting.formatMoney(v, '¥', 0)}`;
        },
      },
    },

    // 状态的属性
    state: {
      active: {
        style: {
          stroke: '#fff',
        },
      },
    },

    // 每个pie的间隔变大
    pieStyle: {
      stroke: '#fff',
      lineWidth: 4,
    },
    // 不在弧度上显示明细
    label: {
      content: '',
    },
    // 图表交互
    interactions: [
      { type: 'element-selected' },
      { type: 'element-active' },
      { type: 'pie-statistic-active' },
    ],
  };
  // @ts-ignore
  return <Pie {...config} />;
};

export default PieSales;
