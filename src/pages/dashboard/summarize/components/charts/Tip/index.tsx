import React from 'react';
import { Badge } from 'antd';

type TipProps = {
  title: string;
  value: number;
  color: string;
};

export default (props: TipProps) => {
  const { title, value, color } = props;
  return (
    <>
      <Badge color={color} />
      <span>{title} </span> <span>{value}</span>
    </>
  );
};
