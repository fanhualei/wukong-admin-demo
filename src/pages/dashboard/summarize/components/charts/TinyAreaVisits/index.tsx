import React from 'react';
import { TinyArea } from '@ant-design/charts';
import Tip from '../Tip';
import { fakeVisitData } from '../../../service';

// 得到模拟的数据
const visitData = fakeVisitData();
const TinyAreaVisits: React.FC = () => {
  // 必须返回一个number[]
  const data = visitData.map((item) => item.value);
  const config = {
    height: 60,
    data,
    smooth: true,
    // 指定填充的颜色
    areaStyle: { fill: '#975fe4' },
    // 为了好看，将线的颜色变成了与背景颜色一样了。
    line: { color: '#fff' },
    // 按照自定义一个组建叫Tip,来显示提示信息
    tooltip: {
      customContent: (index: number) => {
        if (!visitData[index]) {
          return '';
        }
        return <Tip title={visitData[index].date} value={visitData[index].value} color="#975fe4" />;
      },
    },
  };
  // @ts-ignore
  return <TinyArea {...config} />;
};

export default TinyAreaVisits;
