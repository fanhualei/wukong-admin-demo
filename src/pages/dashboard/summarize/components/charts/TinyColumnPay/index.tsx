import React from 'react';
import { TinyColumn } from '@ant-design/charts';
import Tip from '../Tip';
import { fakeVisitData } from '../../../service';

// 得到模拟的数据
const visitData = fakeVisitData();
const TinyColumnPay: React.FC = () => {
  // 必须返回一个number[]
  const data = visitData.map((item) => item.value);
  const config = {
    height: 60,
    data,
    // 按照自定义一个组建叫Tip,来显示提示信息
    tooltip: {
      customContent: (index: number, items: any) => {
        if (!visitData[index]) {
          return '';
        }
        return (
          <Tip
            title={visitData[index].date}
            value={visitData[index].value}
            color={items[0].color}
          />
        );
      },
    },
  };
  // @ts-ignore
  return <TinyColumn {...config} />;
};

export default TinyColumnPay;
