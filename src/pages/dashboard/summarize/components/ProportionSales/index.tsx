import React from 'react';
import { Card, Radio } from 'antd';
import PieSales from '../charts/PieSales';
import {
  fakeSalesTypeDataOffline,
  fakeSalesTypeDataOnline,
  fakeSalesTypeData,
} from '../../service';

import styles from './common.less';

export default () => {
  const [salesType, setSalesType] = React.useState<string>('all');
  const [salesPieData, setSalesPieData] = React.useState<any[]>(fakeSalesTypeData());

  const radioChange = (type: string) => {
    setSalesType(type);
    if (type === 'all') {
      setSalesPieData(fakeSalesTypeData());
    } else if (type === 'all') {
      setSalesPieData(fakeSalesTypeDataOnline());
    } else {
      setSalesPieData(fakeSalesTypeDataOffline());
    }
  };

  return (
    <Card
      title="销售额类别占比"
      style={{ height: '100%' }}
      bordered={false}
      className={styles.customerAntCardExtra}
      extra={
        <div>
          <Radio.Group
            value={salesType}
            onChange={(e) => {
              radioChange(e.target.value);
            }}
          >
            <Radio.Button value="all">全部渠道</Radio.Button>
            <Radio.Button value="online">线上</Radio.Button>
            <Radio.Button value="stores">门店</Radio.Button>
          </Radio.Group>
        </div>
      }
    >
      <div>销售额</div>
      <PieSales data={salesPieData} />
    </Card>
  );
};
