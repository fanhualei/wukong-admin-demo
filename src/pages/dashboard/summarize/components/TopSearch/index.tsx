import React from 'react';
import { Card, Col, Row, Table, Tooltip, Menu, Dropdown } from 'antd';
import { InfoCircleOutlined, EllipsisOutlined } from '@ant-design/icons';
import NumberInfo from '../NumberInfo';
import TinyAreaSearch from '../charts/TinyAreaSearch';
import Trend from '../Trend';
import accounting from 'accounting';
import { fakeVisitData2, fakeSearchData } from '../../service';

const menu = (
  <Menu>
    <Menu.Item>操作一</Menu.Item>
    <Menu.Item>操作二</Menu.Item>
  </Menu>
);

const dropdownGroup = (
  <span>
    <Dropdown overlay={menu} placement="bottomRight">
      <EllipsisOutlined />
    </Dropdown>
  </span>
);

const columns = [
  {
    title: '排名',
    dataIndex: 'index',
    key: 'index',
  },
  {
    title: '搜索关键词',
    dataIndex: 'keyword',
    key: 'keyword',
    render: (text: React.ReactNode) => <a href="#">{text}</a>,
  },
  {
    title: '用户数',
    dataIndex: 'count',
    key: 'count',
    sorter: (a: { count: number }, b: { count: number }) => a.count - b.count,
  },
  {
    title: '周涨幅',
    dataIndex: 'range',
    key: 'range',
    sorter: (a: { range: number }, b: { range: number }) => a.range - b.range,
    render: (text: React.ReactNode, record: { status: number }) => (
      <Trend flag={record.status === 1 ? 'down' : 'up'}>
        <span style={{ marginRight: 4 }}>{text}%</span>
      </Trend>
    ),
  },
];

export default () => {
  //   const record = fakeSearchData();
  return (
    <Card title="线上热门搜索" extra={dropdownGroup}>
      <Row gutter={68} style={{ marginBottom: 10 }}>
        <Col sm={12} xs={24}>
          <NumberInfo
            subTitle={
              <span>
                搜索用户数
                <Tooltip title="指标说明">
                  <InfoCircleOutlined style={{ marginLeft: 8 }} />
                </Tooltip>
              </span>
            }
            gap={8}
            total={accounting.formatNumber(12321)}
            status="up"
            subTotal={17.1}
          />
          <TinyAreaSearch searchData={fakeVisitData2()} />
        </Col>
        <Col sm={12} xs={24}>
          <NumberInfo
            subTitle={
              <span>
                人均搜索次数
                <Tooltip title="指标说明">
                  <InfoCircleOutlined style={{ marginLeft: 8 }} />
                </Tooltip>
              </span>
            }
            gap={8}
            total={2.7}
            status="down"
            subTotal={26.2}
          />
          <TinyAreaSearch searchData={fakeVisitData2()} />
        </Col>
      </Row>

      <Table<any>
        rowKey={(record) => record.index}
        size="small"
        columns={columns}
        dataSource={fakeSearchData()}
        pagination={{
          style: { marginBottom: 0 },
          pageSize: 5,
        }}
      />
    </Card>
  );
};
