import { InfoCircleOutlined } from '@ant-design/icons';
import { Col, Row, Tooltip, Space } from 'antd';
import React from 'react';

import { StatisticCard } from '@ant-design/pro-card';

import styles from './index.less';
import Trend from '../Trend';
import Field from '../Field';
import TinyAreaVisits from '../charts/TinyAreaVisits';
import TinyColumnPay from '../charts/TinyColumnPay';
import TinyProgressActive from '../charts/TinyProgressActive';

const topColResponsiveProps = {
  xs: 24,
  sm: 12,
  md: 12,
  lg: 12,
  xl: 6,
  style: { marginBottom: 24 },
};

const Title = ({ title, desc }: { title: string; desc: string }) => {
  return (
    <div className={styles.title}>
      <span>{title}</span>
      <Tooltip title={desc} className={styles.desc}>
        <InfoCircleOutlined />
      </Tooltip>
    </div>
  );
};

// 定义一个变量，可以传入到valueStyle，这里的color一定要注意写法，可以看index.less
const bodyValueStyle: React.CSSProperties = {
  height: 38,
  marginTop: -10,
  marginBottom: 0,
  overflow: 'hidden',
  fontSize: 30,
  fontWeight: 450,
  color: styles.headingColor,
};

const IntroduceRow = () => (
  <Row gutter={24}>
    <Col {...topColResponsiveProps}>
      <StatisticCard
        style={{ height: 197 }}
        statistic={{
          title: <Title title="总销售额" desc="指标说明" />,
          value: 126560,
          valueStyle: bodyValueStyle,
          prefix: '¥',
        }}
        chart={
          <Space style={{ height: 60 }}>
            <Trend flag="up" style={{ marginRight: 16 }}>
              <span>周同比 </span>
              <span>12%</span>
            </Trend>
            <Trend flag="down">
              <span>日同比 </span>
              <span>11%</span>
            </Trend>
          </Space>
        }
        footer={<Field label="日销售额" value={`￥12,423`} />}
      />
    </Col>
    <Col {...topColResponsiveProps}>
      <StatisticCard
        statistic={{
          title: <Title title="访问量" desc="指标说明" />,
          value: 8846,
          valueStyle: bodyValueStyle,
        }}
        chart={<TinyAreaVisits />}
        footer={<Field label="日访问量" value={`1,234`} />}
      />
    </Col>
    <Col {...topColResponsiveProps}>
      <StatisticCard
        statistic={{
          title: <Title title="支付笔数" desc="指标说明" />,
          value: 6560,
          valueStyle: bodyValueStyle,
        }}
        chart={<TinyColumnPay />}
        footer={<Field label="转换率" value={`60%`} />}
      />
    </Col>
    <Col {...topColResponsiveProps}>
      <StatisticCard
        statistic={{
          title: <Title title="运营活动效果" desc="指标说明" />,
          value: 78,
          valueStyle: bodyValueStyle,
          suffix: '%',
        }}
        chart={<TinyProgressActive />}
        footer={
          <Space>
            <Trend flag="up" style={{ marginRight: 16 }}>
              <span>周同比 </span>
              <span>12%</span>
            </Trend>
            <Trend flag="down">
              <span>日同比 </span>
              <span>11%</span>
            </Trend>
          </Space>
        }
      />
    </Col>
  </Row>
);

export default IntroduceRow;
