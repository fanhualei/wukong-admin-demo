import request from 'umi-request';
import moment from 'moment';

const beginDay = new Date().getTime();

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

// ---------------返回两个缩略图中用到的数据------------------------------------------
export interface VisitDataType {
  date: string;
  value: number;
}

const visitData: VisitDataType[] = [];
const fakeY = [7, 5, 4, 2, 4, 7, 5, 6, 5, 9, 6, 3, 1, 5, 3, 6, 5];
for (let i = 0; i < fakeY.length; i += 1) {
  visitData.push({
    date: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
    value: fakeY[i],
  });
}

export function fakeVisitData() {
  return visitData;
}

export function fakeVisitData2() {
  const visitData2 = [];
  const fakeY2 = [1, 6, 4, 8, 3, 7, 2];
  for (let i = 0; i < fakeY2.length; i += 1) {
    visitData2.push({
      x: moment(new Date(beginDay + 1000 * 60 * 60 * 24 * i)).format('YYYY-MM-DD'),
      y: fakeY2[i] + Math.floor(Math.random() * 10),
    });
  }
  return visitData2;
}

// -----------------------------------------------------------------

// 模拟销售数据
export function fakeSalesData() {
  const salesData = [];
  for (let i = 0; i < 12; i += 1) {
    salesData.push({
      x: `${i + 1}月`,
      y: Math.floor(Math.random() * 1000) + 200,
    });
  }
  return salesData;
}

// 模拟销售排行榜
export function fakeRankingListData() {
  const rankingListData: { title: string; total: number }[] = [];
  for (let i = 0; i < 7; i += 1) {
    rankingListData.push({
      title: `工专路 ${i} 号店`,
      total: 323234,
    });
  }
  return rankingListData;
}

// 模拟检索数据
export function fakeSearchData() {
  const searchData = [];
  for (let i = 0; i < 50; i += 1) {
    searchData.push({
      index: i + 1,
      keyword: `搜索关键词-${i}`,
      count: Math.floor(Math.random() * 1000),
      range: Math.floor(Math.random() * 100),
      status: Math.floor((Math.random() * 10) % 2),
    });
  }
  return searchData;
}

const salesTypeData = [
  {
    x: '家用电器',
    y: 4544,
  },
  {
    x: '食用酒水',
    y: 3321,
  },
  {
    x: '个护健康',
    y: 3113,
  },
  {
    x: '服饰箱包',
    y: 2341,
  },
  {
    x: '母婴产品',
    y: 1231,
  },
  {
    x: '其他',
    y: 1231,
  },
];

export function fakeSalesTypeData() {
  return salesTypeData;
}

const salesTypeDataOnline = [
  {
    x: '家用电器',
    y: 244,
  },
  {
    x: '食用酒水',
    y: 321,
  },
  {
    x: '个护健康',
    y: 311,
  },
  {
    x: '服饰箱包',
    y: 41,
  },
  {
    x: '母婴产品',
    y: 121,
  },
  {
    x: '其他',
    y: 111,
  },
];

export function fakeSalesTypeDataOnline() {
  return salesTypeDataOnline;
}

const salesTypeDataOffline = [
  {
    x: '家用电器',
    y: 99,
  },
  {
    x: '食用酒水',
    y: 188,
  },
  {
    x: '个护健康',
    y: 344,
  },
  {
    x: '服饰箱包',
    y: 255,
  },
  {
    x: '其他',
    y: 65,
  },
];

export function fakeSalesTypeDataOffline() {
  return salesTypeDataOffline;
}

export function fakeOfflineData() {
  const stores = [
    '欧莱雅',
    'SK-II',
    '雅诗兰黛',
    '珀莱雅',
    '御泥坊',
    '百雀羚',
    '香奈儿',
    '兰蔻',
    '美宝莲',
    '倩碧',
  ];
  const offlineData = [];
  for (let i = 0; i < 10; i += 1) {
    offlineData.push({
      name: stores[i],
      cvr: Math.ceil(Math.random() * 9) / 10,
    });
  }
  return offlineData;
}

export function fakeOfflineChartData() {
  const offlineChartData = [];
  for (let i = 0; i < 24; i += 1) {
    const time = i > 9 ? `${i}:00` : `0${i}:00`;
    offlineChartData.push({
      name: 'visit',
      time,
      value: Math.floor(Math.random() * 100) + 10,
    });
    offlineChartData.push({
      name: 'pay',
      time,
      value: Math.floor(Math.random() * 100) + 10,
    });
  }
  return offlineChartData;
}

export interface OfflineDataType {
  name: string;
  cvr: number;
}

export interface OfflineChartData {
  name: string;
  time: string;
  value: number;
}
