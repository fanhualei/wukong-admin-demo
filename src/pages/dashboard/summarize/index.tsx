import React from 'react';
import { Row, Col } from 'antd';
import { GridContent } from '@ant-design/pro-layout';

import IntroduceRow from './components/IntroduceRow';
import SalesCard from './components/SalesCard';
import TopSearch from './components/TopSearch';
import ProportionSales from './components/ProportionSales';
import OfflineData from './components/OfflineData';

const colResponsiveProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 12,
  style: { marginBottom: 24 },
};

const Summarize = () => {
  return (
    <GridContent style={{ padding: 20 }}>
      <React.Fragment>
        <IntroduceRow />
        <SalesCard />
        <Row gutter={24} style={{ marginTop: 24 }}>
          <Col {...colResponsiveProps}>
            <TopSearch />
          </Col>
          <Col {...colResponsiveProps}>
            <ProportionSales />
          </Col>
        </Row>
        <OfflineData />
      </React.Fragment>
    </GridContent>
  );
};

export default Summarize;
