declare namespace Dev {
  export type TableIndex = {
    type: string; // pk 或者 uk
    jsType: string; // js的类型
    columnCamelName: string; // 驼峰型的字段名
  };

  export type ProColumn = {
    proColumnId: number; // id [最大位数:11]
    tableName?: string; // 去掉前缀的驼峰表名 [最大位数:150]
    name?: string; // 名称 [最大位数:50]
    remark?: string; // 备注 [最大位数:50]
    content: string; // 设置的内容
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };
}
