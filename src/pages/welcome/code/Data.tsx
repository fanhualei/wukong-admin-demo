import React from 'react';

import ProCard from '@ant-design/pro-card';
import TableList from './components/TableList';
import TableDetail from './components/TableDetail';
import styles from './code.less';

export default function Data() {
  const [tableName, setTableName] = React.useState<string | undefined>(undefined);
  const [tableComment, setTableComment] = React.useState<string | undefined>(undefined);
  const onTableClicked = (name: string, comment: string) => {
    setTableName(name);
    setTableComment(comment);
  };
  return (
    <ProCard split="vertical">
      <ProCard colSpan="260px" className={styles.customerAntProCardLeft}>
        <TableList onTableClicked={onTableClicked} />
      </ProCard>
      <ProCard headerBordered className={styles.customerAntProCardRight}>
        <TableDetail tableName={tableName} tableComment={tableComment} />
      </ProCard>
    </ProCard>
  );
}
