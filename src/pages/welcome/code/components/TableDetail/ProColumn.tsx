import React from 'react';
import { Button } from 'antd';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import { getProColumnList } from '../../service';
import { getOrderStr, getWheres } from '@/services/Common';

import type { TableDetailProps } from './index';

/** 定义一个`whereNameRules` 转换的规则，其中字段名必须与数据库中的一致。 */
const whereNameRules = {};

export default (props: TableDetailProps) => {
  // 表名和备注
  const { tableName, tableComment } = props;

  // 如果没有选择表，那么就显示一个提示信息
  if (!tableName) {
    return (
      <div>
        <br />
        请在左侧选中要生成代码的表
      </div>
    );
  }

  const columns: ProColumns<Dev.ProColumn>[] = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
  ];

  return (
    <div style={{ minHeight: 360 }}>
      {/* 标题区，这里有复制代码的功能 */}
      <div style={{ marginBottom: 10 }}>
        {tableComment}：{tableName}
      </div>
      <ProTable<Dev.ProColumn>
        columns={columns}
        rowKey="proColumnId"
        search={false}
        options={false}
        toolBarRender={() => [
          <Button key="button" type="primary" onClick={() => {}}>
            新建
          </Button>,
        ]}
        pagination={{
          pageSize: 5,
          size: 'small',
          showSizeChanger: false,
          showTotal: () => <b></b>,
        }}
        request={async (params, sort) => {
          // getWheres 与 getOrderStr 这两个函数，会按照规则，转换成后台能统一识别的查询条件
          const result = await getProColumnList({
            current: params.current,
            pageSize: params.pageSize,
            wheres: getWheres({ ...params, tableName }, whereNameRules),
            order: getOrderStr(sort),
          });
          return result.data;
        }}
        params={{ tableName }}
      />
      大部分时候设置Table与Column都很麻烦，其实中间也有一些规律，如果把这些规律做成向导就方便了，但是现在一直没有想好，这个向导怎么做。
    </div>
  );
};
