import { capitalize, removeLastWord } from '@/utils/stringUtils';

/**
 * 得到主键的信息
 * @param indexList
 * @returns
 */
function getPkIndex(indexList: Dev.TableIndex[]): Dev.TableIndex | undefined {
  for (let i = 0; i < indexList.length; i += 1) {
    const index = indexList[i];
    if (index.type === 'pk') {
      return index;
    }
  }
  return undefined;
}

/**
 * 用来得到某个字段是否存在的代码片段
 * @param pkIndex
 * @param indexList
 */
function getIsExistCode(pkIndex: Dev.TableIndex, indexList: Dev.TableIndex[]): string {
  let ren = '';
  for (let i = 0; i < indexList.length; i += 1) {
    const index = indexList[i];
    if (index.type === 'uk') {
      ren = ren.concat(getOneIsExistCode(pkIndex, index));
    }
  }
  return ren;
}

function getOneIsExistCode(pkIndex: Dev.TableIndex, ukIndex: Dev.TableIndex): string {
  return `

  /**
   * 是否已经存在：${ukIndex.columnCamelName}
   */
  export async function exist${capitalize(ukIndex.columnCamelName)}(${ukIndex.columnCamelName}: ${
    ukIndex.jsType
  }, ${pkIndex.columnCamelName}: ${pkIndex.jsType}) {
    return request<API.ResponseInfo<number>>(\`\${prifix}/exist${capitalize(
      ukIndex.columnCamelName,
    )}\`, {
      method: 'GET',
      params: {
        ${ukIndex.columnCamelName},
        ${pkIndex.columnCamelName},
      },
    });
  }  
  
  `;
}

/**
 * 得到一个完整的service代码，这些代码还需要手工处理
 * @param indexList
 * @returns
 */
export function getServiceCode(
  indexList: Dev.TableIndex[],
  tableName: string,
  tableCommentF: string,
  nameSpace: string,
  subUrl: string,
) {
  const tableComment = removeLastWord(tableCommentF);
  const pkIndex = getPkIndex(indexList);
  if (!pkIndex) {
    throw new Error('error:pkIndex is undefion');
  }
  const isExist = getIsExistCode(pkIndex, indexList);

  return `
    
  import { request } from 'umi';
  import { PrefixUrl } from '@/services/Common';
  
  // -----------------------------常量-------------------------------------------
  
  const prifix = \`\${PrefixUrl}/${subUrl}\`;

  /**
   * 得到 ${tableComment} 列表
   * @param data  可以传入：检索条件、排序、分页等检索条件
   */
  export async function get${capitalize(tableName)}List(data: API.SearchListParams) {
    return request<API.ResponseInfo<API.ResponseListData<${nameSpace}.${capitalize(
    tableName,
  )}>>>(\`\${prifix}/get${capitalize(tableName)}List\`, {
      method: 'POST',
      data,
    });
  }
  
  /**
   * 根据Id得到某个 ${tableComment}
   */
  export async function get${capitalize(tableName)}ById(${pkIndex.columnCamelName}: ${
    pkIndex.jsType
  }) {
    return request<API.ResponseInfo<${nameSpace}.${capitalize(
    tableName,
  )}>>(\`\${prifix}/get${capitalize(tableName)}ById\`, {
      method: 'GET',
      params: {
        ${pkIndex.columnCamelName},
      },
    });
  }  

  /**
   * 根据Id添加或更新某个 ${tableComment}
   */
  export async function save${capitalize(tableName)}(data: ${nameSpace}.${capitalize(tableName)}) {
    return request<API.ResponseInfo<number>>(\`\${prifix}/save${capitalize(tableName)}\`, {
      method: 'POST',
      data,
    });
  }
   
  /**
   * 删除某个${tableComment}
   */
  export async function del${capitalize(tableName)}ById(${pkIndex.columnCamelName}: ${
    pkIndex.jsType
  }) {
    return request<API.ResponseInfo<number>>(\`\${prifix}/del${capitalize(tableName)}ById\`, {
      method: 'GET',
      params: {
        ${pkIndex.columnCamelName},
      },
    });
  }
  
  ${isExist}

    `;
}
