import React from 'react';
import { Tabs } from 'antd';
import DataType from './DataType';
import MyService from './MyService';
import ProColumn from './ProColumn';

const { TabPane } = Tabs;

export type TableDetailProps = {
  tableName: string | undefined;
  tableComment: string | undefined;
};

export default (props: TableDetailProps) => {
  // 没有用的函数
  const callback = (activeKey: string) => {
    console.log(activeKey);
  };

  return (
    <div style={{ minHeight: 360 }}>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="数据结构" key="1">
          <DataType {...props} />
        </TabPane>
        <TabPane tab="Service模板" key="2">
          <MyService {...props} />
        </TabPane>
        <TabPane tab="定义Column" key="3">
          <ProColumn {...props} />
        </TabPane>
      </Tabs>
    </div>
  );
};
