import React from 'react';
import { Tooltip, message, Typography } from 'antd';
import { getDataType } from '../../service';
import { CopyOutlined } from '@ant-design/icons';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import type { TableDetailProps } from './index';

const { Paragraph, Text } = Typography;

export default (props: TableDetailProps) => {
  // 表名和备注
  const { tableName, tableComment } = props;

  // 数据结构信息
  const [dataType, setDataType] = React.useState<String>('');

  // 如果tableName发生变化，就刷新数据
  React.useEffect(() => {
    const fun = async () => {
      const ren = await getDataType(tableName || '');
      if (ren.data) {
        setDataType(ren.data['dataType']);
      }
    };
    if (tableName) {
      fun();
    }
  }, [tableName]);

  // 如果没有选择表，那么就显示一个提示信息
  if (!tableName) {
    return (
      <div>
        <br />
        请在左侧选中要生成代码的表
      </div>
    );
  }

  return (
    <div style={{ minHeight: 360 }}>
      {/* 标题区，这里有复制代码的功能 */}
      <div style={{ marginBottom: 10 }}>
        {tableComment}：{tableName} &nbsp;&nbsp;&nbsp;
        <Tooltip title="复制代码">
          <CopyToClipboard
            text={dataType}
            onCopy={() => {
              message.success('复制成功');
            }}
          >
            <a>
              <CopyOutlined />
            </a>
          </CopyToClipboard>
        </Tooltip>
      </div>
      {/* 代码显示区域 */}
      <SyntaxHighlighter language="typescript" style={docco}>
        {dataType}
      </SyntaxHighlighter>

      <Typography>
        <Paragraph>
          <Text type="secondary">
            使用说明：将这代码复制到所在的d.ts文件，就可以引用了，具体可以参考API.d.ts文件。
          </Text>
        </Paragraph>
      </Typography>
    </div>
  );
};
