import ProList from '@ant-design/pro-list';
import { getTables } from '../../service';
import styles from './index.less';

type TableListProps = {
  onTableClicked: (tableName: string, tableComment: string) => void;
};

export default (props: TableListProps) => {
  const { onTableClicked } = props;
  return (
    <ProList
      className={styles.customerAntList}
      search={{
        filterType: 'light',
      }}
      rowKey="tableName"
      headerTitle="数据库表"
      request={async (params = {}) => {
        const result = await getTables(params['tableName']);
        return result;
      }}
      pagination={{
        pageSize: 10,
        size: 'small',
        showSizeChanger: false,
        showTotal: () => <b></b>,
      }}
      showActions="hover"
      metas={{
        title: {
          dataIndex: 'tableName',
          render: (text, row) => {
            return (
              <a
                className={styles.title}
                onClick={() => {
                  onTableClicked(text as string, row['tableComment']);
                }}
              >
                {text}
              </a>
            );
          },
        },
        description: {
          dataIndex: 'tableComment',
          search: false,
        },
        actions: {
          render: (_, row) => {
            return [
              <a
                onClick={() => {
                  onTableClicked(row['tableName'] as string, row['tableComment'] as string);
                }}
              >
                生成
              </a>,
            ];
          },
          search: false,
        },
      }}
    />
  );
};
