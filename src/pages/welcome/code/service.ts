import { request } from 'umi';

const prifix = `/dev`;
const pwd = 'fanaaa2021';

/**
 * 查询数据库中的表
 * @returns 数据库中表的列表
 */
export async function getTables(tableName: string) {
  return request<API.ResponseInfo<any[]>>(`${prifix}/getTables`, {
    method: 'GET',
    params: {
      pwd,
      tableName,
    },
  });
}

/**
 * 查询数据库中的表
 * @returns 数据库中表的列表
 */
export async function getDataType(tableName: string) {
  return request<API.ResponseInfo<string>>(`${prifix}/getDataType`, {
    method: 'GET',
    params: {
      pwd,
      tableName,
    },
  });
}

export async function getTableIndex(tableName: string) {
  return request<API.ResponseInfo<Dev.TableIndex[]>>(`${prifix}/getTableIndex`, {
    method: 'GET',
    params: {
      pwd,
      tableName,
    },
  });
}

// ---------------------ProColumn设定-------------------------

/**
 * 得到 ProColumn设定内容 列表
 * @param data  可以传入：检索条件、排序、分页等检索条件
 */
export async function getProColumnList(data: API.SearchListParams) {
  return request<API.ResponseInfo<API.ResponseListData<Dev.ProColumn>>>(
    `${prifix}/getProColumnList`,
    {
      method: 'POST',
      data: {
        ...data,
        pwd,
      },
    },
  );
}

/**
 * 根据Id得到某个 ProColumn设定内容
 */
export async function getProColumnById(proColumnId: number) {
  return request<API.ResponseInfo<Dev.ProColumn>>(`${prifix}/getProColumnById`, {
    method: 'GET',
    params: {
      pwd,
      proColumnId,
    },
  });
}

/**
 * 根据Id添加或更新某个 ProColumn设定内容
 */
export async function saveProColumn(data: Dev.ProColumn) {
  return request<API.ResponseInfo<number>>(`${prifix}/saveProColumn`, {
    method: 'POST',
    data: {
      ...data,
      pwd,
    },
  });
}

/**
 * 删除某个ProColumn设定内容
 */
export async function delProColumnById(proColumnId: number) {
  return request<API.ResponseInfo<number>>(`${prifix}/delProColumnById`, {
    method: 'GET',
    params: {
      pwd,
      proColumnId,
    },
  });
}
