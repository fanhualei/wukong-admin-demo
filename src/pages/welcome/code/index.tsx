import { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';

import Data from './Data';
import Swagger from './Swagger';
import Page from './Page';

export default () => {
  const [tabKey, setTabKey] = useState('data');
  return (
    <PageContainer
      className="customPageContainer"
      breadcrumb={undefined}
      onTabChange={(key) => {
        setTabKey(key);
      }}
      tabActiveKey={tabKey}
      tabList={[
        {
          tab: '数据对象',
          key: 'data',
        },
        {
          tab: 'Swagger',
          key: 'swagger',
        },
        {
          tab: '页面向导',
          key: 'page',
        },
      ]}
    >
      {tabKey === 'data' && <Data />}
      {tabKey === 'swagger' && <Swagger />}
      {tabKey === 'page' && <Page />}
    </PageContainer>
  );
};
