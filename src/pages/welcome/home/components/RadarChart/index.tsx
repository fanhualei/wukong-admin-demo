import React from 'react';
import { Radar } from '@ant-design/charts';
import { fakeRadarData } from './service';

const DemoRadar: React.FC = () => {
  const data = fakeRadarData();

  const config = {
    data,
    xField: 'label',
    yField: 'value',
    seriesField: 'name',
    xAxis: {
      line: null,
      tickLine: null,
      grid: {
        line: {
          style: {
            lineDash: null,
          },
        },
      },
    },
    yAxis: {
      line: null,
      tickLine: null,
      grid: {
        line: {
          type: 'line',
          style: {
            lineDash: null,
          },
        },
      },
    },
    // 开启面积
    area: {},
    // 开启辅助点
    point: {},
  };

  return <Radar {...config} />;
};

export default DemoRadar;
