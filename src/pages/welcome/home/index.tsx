/**
 * 这个页面是复制的antDesignPro的页面
 * 因为今后要重新制作，所以这个页面里面的代码写的有点乱，也没有重构了。
 * 有以下的内容，今后可以重用：
 * 1：快捷键的设置这部分。
 * 2：显示头像的这部分
 */

import { Avatar, Card, Col, List, Skeleton, Row, Statistic } from 'antd';
import React from 'react';

import { Link, useModel } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import moment from 'moment';

import EditableLinkGroup from './components/EditableLinkGroup';
import styles from './style.less';
import type { ActivitiesType, CurrentUser, NoticeType } from './data.d';
import { queryCurrent, queryProjectNotice, queryActivities } from './service';

import { getShortcutFromStore } from '@/layouts/nested/SubSider/utils';

import { SettingOutlined } from '@ant-design/icons';

import ShortcutSetting from '@/components/ShortcutSetting';

import RadarChart from './components/RadarChart';

interface WorkplaceProps {
  currentUser?: CurrentUser;
  projectNotice?: NoticeType[];
  activities?: ActivitiesType[];
  currentUserLoading: boolean;
  projectLoading: boolean;
  activitiesLoading: boolean;
}

/**
 * 标题
 * @param param0
 * @returns
 */
const PageHeaderContent: React.FC<{ currentUser: CurrentUser }> = ({ currentUser }) => {
  const loading = currentUser && Object.keys(currentUser).length;
  if (!loading) {
    return <Skeleton avatar paragraph={{ rows: 1 }} active />;
  }
  return (
    <div className={styles.pageHeaderContent}>
      <div className={styles.avatar}>
        <Avatar size="large" src={currentUser.avatar} />
      </div>
      <div className={styles.content}>
        <div className={styles.contentTitle}>
          早安，
          {currentUser.name}
          ，祝你开心每一天！
        </div>
        <div>
          {currentUser.title} |{currentUser.group}
        </div>
      </div>
    </div>
  );
};

/**
 * 扩展信息
 * @returns
 */
const ExtraContent: React.FC<{}> = () => (
  <div className={styles.extraContent}>
    <div className={styles.statItem}>
      <Statistic title="项目数" value={56} />
    </div>
    <div className={styles.statItem}>
      <Statistic title="团队内排名" value={8} suffix="/ 24" />
    </div>
    <div className={styles.statItem}>
      <Statistic title="项目访问" value={2223} />
    </div>
  </div>
);

const renderActivities = (item: ActivitiesType) => {
  const events = item.template.split(/@\{([^{}]*)\}/gi).map((key) => {
    if (item[key]) {
      return (
        <a href={item[key].link} key={item[key].name}>
          {item[key].name}
        </a>
      );
    }
    return key;
  });

  return (
    <List.Item key={item.id}>
      <List.Item.Meta
        avatar={<Avatar src={item.user.avatar} />}
        title={
          <span>
            <a className={styles.username}>{item.user.name}</a>
            &nbsp;
            <span className={styles.event}>{events}</span>
          </span>
        }
        description={
          <span className={styles.datetime} title={item.updatedAt}>
            {moment(item.updatedAt).fromNow()}
          </span>
        }
      />
    </List.Item>
  );
};

export default () => {
  const [data, setData] = React.useState<WorkplaceProps>({
    projectLoading: true,
    currentUserLoading: true,
    activitiesLoading: true,
  });

  // 弹出的快捷菜单设计页面
  const [editVisible, setEditVisible] = React.useState(false);

  const { initialState } = useModel('@@initialState');

  React.useEffect(() => {
    const currentUser = queryCurrent();

    if (initialState?.currentUser) {
      setData({
        currentUser: {
          ...currentUser,
          avatar:
            initialState.currentUser.user.avatar ||
            'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
          name: initialState.currentUser.user.alias || initialState.currentUser.user.adminName,
        },
        activities: queryActivities(),
        projectNotice: queryProjectNotice(),
        projectLoading: false,
        currentUserLoading: false,
        activitiesLoading: false,
      });
    }
  }, []);

  const { currentUser, activities, projectNotice, projectLoading, activitiesLoading } = data;

  if (!currentUser || !currentUser.userid) {
    return null;
  }

  // 从本地缓存得到快捷菜单
  const links = getShortcutFromStore();

  return (
    <PageContainer
      header={{
        title: undefined,
      }}
      breadcrumb={undefined}
      content={<PageHeaderContent currentUser={currentUser} />}
      extraContent={<ExtraContent />}
    >
      <Row gutter={24}>
        <Col xl={16} lg={24} md={24} sm={24} xs={24}>
          <Card
            className={styles.projectList}
            style={{ marginBottom: 24 }}
            title="进行中的项目"
            bordered={false}
            extra={<Link to="/">全部项目</Link>}
            loading={projectLoading}
            bodyStyle={{ padding: 0 }}
          >
            {projectNotice?.map((item: any) => (
              <Card.Grid className={styles.projectGrid} key={item.id}>
                <Card bodyStyle={{ padding: 0 }} bordered={false}>
                  <Card.Meta
                    title={
                      <div className={styles.cardTitle}>
                        <Avatar size="small" src={item.logo} />
                        <Link to={item.href}>{item.title}</Link>
                      </div>
                    }
                    description={item.description}
                  />
                  <div className={styles.projectItemContent}>
                    <Link to={item.memberLink}>{item.member || ''}</Link>
                    {item.updatedAt && (
                      <span className={styles.datetime} title={item.updatedAt}>
                        {moment(item.updatedAt).fromNow()}
                      </span>
                    )}
                  </div>
                </Card>
              </Card.Grid>
            ))}
          </Card>
          <Card
            bodyStyle={{ padding: 0 }}
            bordered={false}
            className={styles.activeCard}
            title="动态"
            loading={activitiesLoading}
          >
            <List<ActivitiesType>
              loading={activitiesLoading}
              renderItem={(item) => renderActivities(item)}
              dataSource={activities}
              className={styles.activitiesList}
              size="large"
            />
          </Card>
        </Col>
        <Col xl={8} lg={24} md={24} sm={24} xs={24}>
          <Card
            style={{ marginBottom: 24 }}
            title="快速开始 / 便捷导航"
            bordered={false}
            bodyStyle={{ padding: 0 }}
            extra={
              <SettingOutlined
                onClick={() => {
                  setEditVisible(true);
                }}
              />
            }
          >
            <EditableLinkGroup onAdd={() => {}} links={links || []} linkElement={Link} />
          </Card>
          <Card title="活跃指数" style={{ marginBottom: 24 }}>
            <RadarChart />
          </Card>
          <Card
            bodyStyle={{ paddingTop: 12, paddingBottom: 12 }}
            bordered={false}
            title="团队"
            loading={projectLoading}
          >
            <div className={styles.members}>
              <Row gutter={48}>
                {projectNotice?.map((item: any) => (
                  <Col span={12} key={`members-item-${item.id}`}>
                    <Link to={item.href}>
                      <Avatar src={item.logo} size="small" />
                      <span className={styles.member}>{item.member}</span>
                    </Link>
                  </Col>
                ))}
              </Row>
            </div>
          </Card>
        </Col>
      </Row>
      {/* 快捷菜单设置组件 */}
      <ShortcutSetting visible={editVisible} listReload={() => {}} setVisible={setEditVisible} />
    </PageContainer>
  );
};
