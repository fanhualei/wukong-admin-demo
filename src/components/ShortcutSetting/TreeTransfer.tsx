import React from 'react';
import { Transfer, Tree } from 'antd';

import type { DataNode } from 'rc-tree/lib/interface';
import styles from './index.less';

// Customize Table Transfer
const isChecked = (selectedKeys: string[], eventKey: any) => selectedKeys.indexOf(eventKey) !== -1;

/**
 * 这还是一个递归函数,用来初始化树形结构
 * @param treeNodes
 * @param checkedKeys
 * @returns
 */
const generateTree = (treeNodes: DataNode[] = [], checkedKeys: string[] = []): DataNode[] =>
  treeNodes.map(({ children, ...props }) => ({
    ...props,
    children: generateTree(children, checkedKeys),
  }));

/**
 * 主函数
 * @param param0
 * @returns
 */
type TreeTransferProps = {
  dataSource: DataNode[];
  targetKeys: string[];
  onChange: (keys: string[]) => void;
};

/**
 * 虽然这里只有一个参数，但是还是使用了...restProps，主要是为了今后好扩展，这样可以少写一些参数
 */
const TreeTransfer = ({ dataSource, targetKeys, ...restProps }: TreeTransferProps) => {
  const transferDataSource: any[] = [];

  function flatten(list: DataNode[] = []) {
    list.forEach((item) => {
      transferDataSource.push(item);
      flatten(item.children);
    });
  }
  flatten(dataSource);

  /**
   * 树形选择框
   * @param direction 左边还是右边的列表 left | right
   * @param onItemSelect 勾选条目，这里是一个函数 (key: string, selected: boolean)
   * @param selectedKeys 选中的条目 string[]
   * @returns 树形选择框
   */
  const showTree = (
    direction: 'left' | 'right',
    onItemSelect: (key: string, selected: boolean) => void,
    selectedKeys: string[],
  ) => {
    // 如果是左边的选择框
    if (direction === 'left') {
      const checkedKeys = [...selectedKeys, ...targetKeys];
      return (
        <Tree
          blockNode
          checkable
          checkStrictly
          defaultExpandAll
          height={363}
          // checkedKeys 是记录左侧被选中的组件有那些
          checkedKeys={checkedKeys}
          treeData={generateTree(dataSource, targetKeys)}
          onCheck={(_, { node: { key } }) => {
            onItemSelect(key as string, !isChecked(checkedKeys, key));
          }}
          onSelect={(_, { node: { key } }) => {
            onItemSelect(key as string, !isChecked(checkedKeys, key));
          }}
        />
      );
    }
    // 如果是右边的选择框，那么返回undefined，表示按照默认的显示
    return undefined;
  };

  return (
    <>
      <Transfer
        {...restProps}
        targetKeys={targetKeys}
        dataSource={transferDataSource}
        className="tree-transfer"
        render={(item) => item.title}
        showSelectAll={false}
        style={{ height: 400 }}
      >
        {/* 显示树形组件   */}
        {({ direction, onItemSelect, selectedKeys }) =>
          showTree(direction, onItemSelect, selectedKeys)
        }
      </Transfer>
      <span className={styles.desc}>最多只能选中8个快捷菜单</span>
    </>
  );
};

export default TreeTransfer;
