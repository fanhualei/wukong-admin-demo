import React from 'react';
import { Collapse } from 'antd';

const { Panel } = Collapse;

type HelpProps = {
  children: React.ReactNode;
};

const Help: React.FC<HelpProps> = (props: HelpProps) => {
  const { children } = props;
  return (
    <Collapse
      className="customCollapse"
      defaultActiveKey={['1']}
      bordered={false}
      style={{ marginBottom: 15 }}
    >
      <Panel header="操作提示" key="1">
        {children}
      </Panel>
    </Collapse>
  );
};

export default Help;
