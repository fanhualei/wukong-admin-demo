import React from 'react';
import Field from '@ant-design/pro-field';

type MoneyProps = {
  text: string | number;
};

const Money: React.FC<MoneyProps> = (props: MoneyProps) => {
  const { text } = props;
  return (
    <div style={{ color: 'red', display: 'inline' }}>
      <Field text={text} mode="read" valueType="money" />
    </div>
  );
};

export default Money;
