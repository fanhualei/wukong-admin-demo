import React from 'react';
import { Switch } from 'antd';

type WkSwitchProps = {
  value?: boolean;
  onChange?: (checked: boolean) => void;
  checkedChildren: string;
  unCheckedChildren: string;
};

/**
 * 由于在editTable中，没有value与onchange，这样就不能灵活与editTable适配，所以就做了这个功能
 */
export default (props: WkSwitchProps) => {
  const { value, onChange, checkedChildren, unCheckedChildren } = props;
  return (
    <Switch
      checkedChildren={checkedChildren}
      unCheckedChildren={unCheckedChildren}
      onChange={onChange}
      defaultChecked={value}
    />
  );
};
