﻿/**
 * 由于Pro有bug,我复制了他的代码：https://github.com/ant-design/pro-components/blob/master/packages/table/src/components/EditableTable/index.tsx
 * 然后修改了167的代码
 */

import React from 'react';

const WkEditTable = () => {
  return (
    <div className="ant-table-wrapper">
      aaaa
      <div className="ant-spin-nested-loading">
        <div className="ant-spin-container">
          <div className="ant-table ant-table-middle">
            <div className="ant-table-container">
              <div className="ant-table-content">
                <table style={{ tableLayout: 'auto' }}>
                  <colgroup>
                    <col style={{ width: '20%', minWidth: '20%' }} />
                    <col style={{ width: '10%', minWidth: '10%' }} />
                    <col style={{ width: '60%', minWidth: '60%' }} />
                  </colgroup>
                  <thead className="ant-table-thead">
                    <tr>
                      <th className="ant-table-cell">规则</th>
                      <th className="ant-table-cell">获得经验值</th>
                      <th className="ant-table-cell"></th>
                    </tr>
                  </thead>

                  <tbody className="ant-table-tbody">
                    <tr data-row-key="exp_login" className="ant-table-row ant-table-row-level-0">
                      <td className="ant-table-cell">会员每天第一次登录</td>
                      <td className="ant-table-cell">会员每天第一次登录</td>
                      <td className="ant-table-cell">会员每天第一次登录</td>
                    </tr>
                    <tr data-row-key="exp_login" className="ant-table-row ant-table-row-level-0">
                      <td className="ant-table-cell">会员每天第一次登录</td>
                      <td className="ant-table-cell">会员每天第一次登录</td>
                      <td className="ant-table-cell">会员每天第一次登录</td>
                    </tr>
                  </tbody>
                </table>
                <div className="ant-table-footer">ddddd</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WkEditTable;
