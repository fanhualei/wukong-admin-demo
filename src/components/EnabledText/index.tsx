import React from 'react';
import { CheckCircleFilled, CloseCircleFilled } from '@ant-design/icons';
import styles from './index.less';

interface EnabledTextProps {
  enabled: boolean | undefined | React.ReactNode;
}

const EnabledText: React.FC<EnabledTextProps> = ({ enabled }) => {
  if (enabled) {
    return <CheckCircleFilled className={styles.success} />;
  }
  return <CloseCircleFilled className={styles.error} />;
};

export default EnabledText;
