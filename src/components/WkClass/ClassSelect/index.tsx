import React from 'react';
import { Spin, Cascader } from 'antd';
import { getClassTree, getClassTreeAndParentById } from '@/services/goods/class';

interface ClassSelectProps {
  value?: number;
  onChange?: (value: number | undefined) => void;
}

/**
 * 这个组件的作用是为了显示一级组件，可以返回classId与className,所以在外围form中，就不用定义classId与className了
 * @param props 参数
 */
const ClassSelect: React.FC<ClassSelectProps> = (props: ClassSelectProps) => {
  const { value, onChange } = props;

  // 得到商品分类下拉框的数据
  const [dataSource, setDataSource] = React.useState<any[] | undefined>(undefined);
  const [defaultValue, setDefaultValue] = React.useState<(string | number)[]>([]);

  /**
   * 读取取数据库中的数据，并刷新页面
   */
  React.useEffect(() => {
    const initFun = async () => {
      // 根据当前编号，父节点的编号数组
      const classResult = await getClassTreeAndParentById(value || 0);
      if (classResult.success) {
        setDefaultValue(classResult.data.classIds);
      }

      // 检索出数据
      const result = await getClassTree();
      if (result.success) {
        setDataSource(result.data.children);
      }
    };
    initFun();
  }, [value]); // 这里一定要设置，不然不能正确显示出内容

  const width = { width: '328px' };

  if (!dataSource) {
    return <Spin />;
  }

  return (
    <Cascader
      style={{ ...width }}
      options={dataSource}
      defaultValue={defaultValue}
      onChange={(valueArray) => {
        if (onChange && valueArray && valueArray.length > 0) {
          // 回调函数
          // @ts-ignore
          onChange(valueArray[valueArray.length - 1]);
        }
      }}
      allowClear
      changeOnSelect
    />
  );
};

ClassSelect.defaultProps = {};

export default ClassSelect;
