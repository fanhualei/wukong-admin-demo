import React from 'react';
import { Upload, message, Button, Image } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { getToken } from '@/services/tokenInfo';
import type { UploadChangeParam, RcFile } from 'antd/lib/upload/interface';
import './index.less';

type WkUploadImgProps = {
  name: string;
  label: string;
  title: string;
  extra?: React.ReactNode;
  initSrc: string;
  initImgName: string;
  onUploadEnd: (values: any) => void; // 将保持的图片信息，传递给父组件
  width?: 'lg' | 'md';
};

// 上传前的校验
const beforeUpload = (file: RcFile) => {
  const isJpgOrPng: boolean = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error(`只能上传JPG/PNG文件：${file.name}`);
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('上传文件必须小于2M');
  }
  return isJpgOrPng && isLt2M;
};

const WkUploadImg: React.FC<WkUploadImgProps> = (props: WkUploadImgProps) => {
  const [src, setSrc] = React.useState<string | undefined>(undefined);
  const [imgName, setImgName] = React.useState<string | undefined>(undefined);

  const { label, title, extra, initSrc, initImgName, name, onUploadEnd, width } = props;

  // 根据width来返回大小
  const getWidth = () => {
    if (width === 'lg') {
      return 440;
    }
    return 328;
  };

  // 图片改变
  const onChange = (info: UploadChangeParam) => {
    if (info.file.status === 'done') {
      setImgName(info.file.name);
      setSrc(info.file.response);
      message.success(`文件成功上传到临时文件夹`);
      const ren = {};
      ren[name] = info.file.response;
      onUploadEnd(ren);
    } else if (info.file.status === 'error') {
      message.error(`文件上传失败：${info.file.name} `);
    }
  };

  return (
    <div className="ant-form-vertical">
      <div className="ant-row ant-form-item" style={{ rowGap: 0 }}>
        <div className="ant-col ant-form-item-label">
          <label>{label}</label>
        </div>
        <div className="ant-col ant-form-item-control">
          <Upload
            showUploadList={false}
            name="file"
            action="/api/admin/upload/temp"
            headers={{ Authorization: getToken() as string }}
            beforeUpload={beforeUpload}
            onChange={onChange}
          >
            <Button icon={<UploadOutlined />}>{title}</Button>
          </Upload>
          <div className="picture-list" style={{ width: getWidth() }}>
            <Image width={48} height={48} src={src || initSrc} />
            {/* fallback={ImgFallback} */}
            <label
              className="ant-upload-list-item-name"
              style={{ lineHeight: '44px', textOverflow: 'ellipsis', color: '#1890FF' }}
              title={imgName || initImgName}
            >
              {imgName || initImgName}
            </label>
          </div>

          {/* <div className="ant-form-item-explain ant-form-item-explain-error">请输入网站名称</div> */}

          {extra && <div className="ant-form-item-extra">{extra}</div>}
        </div>
      </div>
    </div>
  );
};

export default WkUploadImg;
