import DndPicturesWall from './DndPicturesWall';
import type { refProps as tempRefProps } from './DndPicturesWall';

export default DndPicturesWall;

export type refProps = tempRefProps;
