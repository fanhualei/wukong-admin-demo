import { request } from 'umi';
import { PrefixUrl, convertToOptions } from '@/services/Common';

// -----------------------------常量-------------------------------------------

const prifix = `${PrefixUrl}/system/admin`;

// -----------------------------管理员-------------------------------------------

/**
 * 管理员列表
 * @param data  参数 可以传入分页
 */
export async function getAdminList(data: API.SearchListParams) {
  return request<API.ResponseInfo<API.ResponseListData<API.Admin>>>(`${prifix}/getAdminList`, {
    method: 'POST',
    data,
  });
}

/**
 * 根据id得到管理员
 */
export async function getAdminById(adminId: string) {
  return request<API.ResponseInfo<API.Admin>>(`${prifix}/getAdminById`, {
    method: 'GET',
    params: {
      adminId,
    },
  });
}

/**
 * 根据id删除管理员
 */
export async function delAdminById(adminId: string) {
  return request<API.ResponseInfo<number>>(`${prifix}/delAdminById`, {
    method: 'GET',
    params: {
      adminId,
    },
  });
}

/**
 * 更新管理员
 */
export async function updateAdmin(admin: Partial<API.Admin>) {
  return request<API.ResponseInfo<number>>(`${prifix}/updateAdmin`, {
    method: 'POST',
    data: {
      ...admin,
    },
  });
}

/**
 * 管理员名称是否重复
 */
export async function existAdminName(adminName: string, adminId: string) {
  return request<API.ResponseInfo<boolean>>(`${prifix}/existAdminName`, {
    method: 'GET',
    params: {
      adminName,
      adminId,
    },
  });
}

// -----------------------------权限组-------------------------------------------

/**
 * 权限组列表
 */
export async function getAdminGroupList() {
  return request<API.ResponseInfo<API.AdminGroup[]>>(`${prifix}/getAdminGroupList`, {
    method: 'GET',
  });
}

export type AdminGroupItem = {
  cname?: string;
  path: string;
};

export type AdminGroupTree = {
  cname?: string;
  path: string;
  children: AdminGroupItem[];
};

/**
 * 得到菜单tree数据结构
 * 权限组界面设置时用到
 */
export async function getAdminGroupTree() {
  return request<API.ResponseInfo<AdminGroupTree[]>>(`${prifix}/getAdminGroupTree`, {
    method: 'GET',
  });
}

/**
 * 根据id得到权限组
 */
export async function getAdminGroupById(groupId: number) {
  return request<API.ResponseInfo<API.AdminGroup>>(`${prifix}/getAdminGroupById`, {
    method: 'GET',
    params: {
      groupId,
    },
  });
}

/**
 * 得到当前用户的权限信息
 */
export async function getCurrentAdminGroup() {
  return request<API.ResponseInfo<API.AdminGroup>>(`${prifix}/getCurrentAdminGroup`, {
    method: 'GET',
  });
}

/**
 * 根据id删除权限组
 */
export async function delAdminGroupById(groupId: number) {
  return request<API.ResponseInfo<number>>(`${prifix}/delAdminGroupById`, {
    method: 'GET',
    params: {
      groupId,
    },
  });
}

/**
 * 更新权限组
 */
export async function updateAdminGroup(adminGroup: API.AdminGroup) {
  return request<API.ResponseInfo<number>>(`${prifix}/updateAdminGroup`, {
    method: 'POST',
    data: {
      ...adminGroup,
    },
  });
}

/**
 * 更新权限组
 */
export async function existGroupName(groupName: string, groupId: number) {
  return request<API.ResponseInfo<boolean>>(`${prifix}/existGroupName`, {
    method: 'GET',
    params: {
      groupName,
      groupId,
    },
  });
}

// -----------------------------工具类-------------------------------------------

/**
 * 将权限列表转换成 select table下拉框可以读取的格式。
 * @param initValues 不是从数据库中读取的一些数值，例如下拉框中的全部。
 */
export async function getAdminGroupListOptions(initValues: any[]) {
  const result = await getAdminGroupList();
  return convertToOptions(initValues, result.data, 'groupName', 'groupId');
}
