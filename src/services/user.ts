import { request } from 'umi';
import { PrefixUrl } from '@/services/Common';

export async function query() {
  return request<API.CurrentUser[]>('/api/users');
}

// 得到当前的用户信息
export async function queryCurrent() {
  return request<API.ResponseInfo<API.CurrentUser>>(`${PrefixUrl}/currentUser`);
}

// 保存当前的用户的快捷菜单
export async function saveShortcut(shortcut: string) {
  return request<API.ResponseInfo<number>>(`${PrefixUrl}/saveShortcut`, {
    method: 'GET',
    params: {
      shortcut,
    },
  });
}

export async function queryNotices(): Promise<any> {
  return request<{ data: API.NoticeIconData[] }>('/api/notices');
}
