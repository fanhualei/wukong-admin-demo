import { request } from 'umi';

export type LoginParamsType = {
  username: string;
  password: string;
  mobile: string;
  captcha: string;
  type: string;
};

export async function OauthLogin(params: LoginParamsType) {
  return request<API.ResponseInfo<API.TokenInfo>>('/oauth/token', {
    method: 'POST',
    params: {
      ...params,
    },
  });
}

/**
 * 这个函数还没有实现
 * @param mobile
 * @returns
 */
export async function getFakeCaptcha(mobile: string) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

/**
 * 调用服务器的推出登录函数，实际上服务器上啥也没有处理，所以总是返回的1
 * @returns 1
 */
export async function logout() {
  return request('/oauth/logout');
}
