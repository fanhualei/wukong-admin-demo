// 用来保存token,读取token 删除token
// 将当前登录用户的信息，保存到浏览器的缓存中，默认是`sessionStorage`，如果是`记住我`，就保存到`storage`中。

import sessionStorage from 'store/storages/sessionStorage';

import store from 'store';

/**
 * 保存用户登录成功后的信息
 * @param rememberMe 是否记住我
 * @param tokenInfo  用户登录后的信息
 */
export function saveInfo(rememberMe: boolean, tokenInfo: API.TokenInfo) {
  // 保存到sessionStorage
  sessionStorage.write('tokenInfo', JSON.stringify(tokenInfo));
  // 保存到localStorage
  if (rememberMe) {
    store.set('tokenInfo', tokenInfo);
  }
}

/**
 * 得到用户TokenInfo信息
 * @returns 是一个数据结构，里面有用户名称、token等信息。
 */
export function getInfo(): API.TokenInfo | undefined {
  const infoStr = sessionStorage.read('tokenInfo');
  // 如果没有，就去localStorage中找
  if (!infoStr) {
    const infoObj: API.TokenInfo = store.get('tokenInfo');
    return infoObj;
  }
  const infoObj: API.TokenInfo = JSON.parse(infoStr);
  return infoObj;
}

/**
 * 得到Token字符串，这样就不用再麻烦的解析TokenInfo对象了。实际上调用了getInfo函数
 * @returns token 字符串
 */
export function getToken(): string | undefined {
  const info = getInfo();
  if (!info) {
    return undefined;
  }
  return info.token;
}
