declare namespace API {
  // ------------------底层接口--------------------------
  export type ResponseInfo<T> = {
    success: boolean;
    data: T;
    host: string;
    timestamp: Date;

    status?: number;
    errorCode?: number;
    errorMessage?: string;
    showType?: number;
    traceId?: string;
    path?: string;
    exception?: string;
    errors?: unknown;
  };

  export type ResponseListData<T> = {
    data: T[];
    success: boolean;
    total: number;
    // 下面两个属性是备用，作用不大
    current?: number;
    pageSize?: number;
  };

  // 与后台统一的
  export type SearchListParams = {
    wheres?: unknown;
    current?: number;
    pageSize?: number;
    order?: string;
    select?: string;
  };

  export type TokenInfo = {
    refresh_token?: string;
    user_name: string;
    active: boolean;
    exp: number;
    token: string;
  };

  // 编辑页面，抽屉页面要传入的参数
  export type EditPageProps = {
    visible: boolean;
    id: number | string;
    setVisible: (visible: boolean) => void;
    listReload: () => void;
  };

  // ------------------实体类接口--------------------------

  export type AdminGroup = {
    groupId: number; // 自增id
    groupName?: string; // 组名
    limits?: string; // 权限内容
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };
  export type AdminLog = {
    id: string; // id
    content: string; // 操作内容
    createtime?: string; // 发生时间
    adminName: string; // 管理员
    adminId: string; // 管理员ID
    ip: string; // IP
    url: string; // act&op
  };
  export type AdminRoute = {
    id: number; // id
    cname: string; // 中文名称
    name: string; // 名称，用来多语言标记
    path: string; // 前台的url路径，后台controller也通用
    parentId: number; // 上个节点编号
    hideInMenu: boolean; // false：不需要在前台显示
    autoAdd: boolean; // true：后台controller的路径，这个不出现在前台，并且在后台自动添加
  };
  export type Admin = {
    adminId: string; // 管理员ID
    adminName: string; // 管理员名称
    alias?: string; // 别名
    adminPassword: string; // 管理员密码
    adminLoginTime?: string; // 登录时间
    adminLoginNum: number; // 登录次数
    adminIsSuper: boolean; // 是否超级管理员
    enabled?: boolean; // 是否有效
    phone?: string; // 手机号码
    email?: string; // 邮箱地址
    avatar?: string; // 头像
    adminGid?: number; // 权限组ID
    tags?: string; // 标签
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // -------------------会员相关 memeber----------------------

  export type Member = {
    memberId: string; // 会员id [最大位数:20]
    memberName: string; // 会员名称 [最大位数:50]
    memberTruename?: string; // 真实姓名 [最大位数:20]
    memberAvatar?: string; // 会员头像 [最大位数:50]
    memberSex?: number; // 会员性别 [最大位数:2]
    memberBirthday?: string; // 生日
    memberPasswd: string; // 会员密码 [最大位数:100]
    memberPaypwd?: string; // 支付密码 [最大位数:100]
    memberEmail?: string; // 会员邮箱 [最大位数:100]
    memberEmailBind: boolean; // 0未绑定1已绑定 [最大位数:1]
    memberMobile?: string; // 手机号 [最大位数:20]
    memberMobileBind: boolean; // 0未绑定1已绑定 [最大位数:1]
    memberQq?: string; // qq [最大位数:100]
    memberWw?: string; // 阿里旺旺 [最大位数:100]
    memberLoginNum?: number; // 登录次数 [最大位数:11]
    memberTime: string; // 会员注册时间
    memberLoginTime?: string; // 当前登录时间
    memberOldLoginTime?: string; // 上次登录时间
    memberLoginIp?: string; // 当前登录ip [最大位数:20]
    memberOldLoginIp?: string; // 上次登录ip [最大位数:20]
    memberQqopenid?: string; // qq互联id [最大位数:100]
    memberQqinfo?: string; // qq账号相关信息
    memberSinaopenid?: string; // 新浪微博登录id [最大位数:100]
    memberSinainfo?: string; // 新浪账号相关信息序列化值
    memberPoints: number; // 会员积分 [最大位数:11]
    availablePredeposit: number; // 预存款可用金额 [最大位数:10,2]
    freezePredeposit: number; // 预存款冻结金额 [最大位数:10,2]
    availableRcBalance: number; // 可用充值卡余额 [最大位数:10,2]
    freezeRcBalance: number; // 冻结充值卡余额 [最大位数:10,2]
    informAllow: boolean; // 是否允许举报(1可以/0不可以) [最大位数:1]
    isBuy: boolean; // 会员是否有购买权限 1为开启 0为关闭 [最大位数:1]
    isAllowtalk: boolean; // 会员是否有咨询和发送站内信的权限 1为开启 0为关闭 [最大位数:1]
    memberState: boolean; // 会员的开启状态 1为开启 0为关闭 [最大位数:1]
    memberSnsvisitnum: number; // sns空间访问次数 [最大位数:11]
    memberAreaid?: number; // 地区ID [最大位数:11]
    memberCityid?: number; // 城市ID [最大位数:11]
    memberProvinceid?: number; // 省份ID [最大位数:11]
    memberAreainfo?: string; // 地区内容 [最大位数:255]
    memberPrivacy?: string; // 隐私设定
    memberQuicklink?: string; // 会员常用操作 [最大位数:255]
    memberExppoints: number; // 会员经验值 [最大位数:11]
    memberWxopenid?: string; // 微信id [最大位数:100]
    memberWxinfo?: string; // 微信账号相关信息
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 会员级别
  export type MemberGrade = {
    level: number;
    levelName: string;
    expPoints: number;
  };

  // 设置的一个标准属性 对应的时ec_setting表的json字段
  export type SettingJsonNumber = {
    id: number;
    rowId: string;
    value: number;
    desc?: string;
  };

  // 经验值日志
  export type ExpPointsLog = {
    expId: string; // 经验值日志编号 [最大位数:20]
    memberId: string; // 会员编号 [最大位数:20]
    memberName: string; // 会员名称 [最大位数:100]
    expPoints: number; // 经验值负数表示扣除 [最大位数:11]
    expDesc: string; // 操作描述 [最大位数:200]
    expStage: string; // 操作阶段 [最大位数:100]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 发送消息
  export type MessageSend = {
    messageSendId: string; // 短消息索引id [最大位数:20]
    messageSendParentId: string; // 回复短消息message_id [最大位数:20]
    sendScope: number; // 1为个人,  2为全体   3为指定规则 4为多人但是不超过100人 [最大位数:11]
    sendMemberId: string; // 短消息发送人 [最大位数:20]
    toMemberId?: string; // 短消息接收人 [最大位数:20]
    toMemberNames?: string; // 4为多人但是不超过100人 短消息接收人 [最大位数:1000]
    toMemberRule?: string; // 3为指定规则 [最大位数:1000]
    title?: string; // 短消息标题 [最大位数:50]
    body: string; // 短消息内容 [最大位数:500]
    sendTime: string; // 短消息发送时间
    sendState: number; // 短消息状态，0为草稿  1为等待发送  2为已经发送  3为删除 [最大位数:11]
    regularTime?: string; // 预约发送时间
    messageType: number; // 0为私信、1为系统消息、2为留言 [最大位数:11]
    fromMemberName?: string; // 发信息人用户名 [最大位数:100]
    toMemberName?: string; // 接收人用户名 [最大位数:100]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 积分日志
  export type PointsLog = {
    pointsLogId: string; // 积分日志编号 [最大位数:20]
    memberId: string; // 会员编号 [最大位数:20]
    memberName: string; // 会员名称 [最大位数:100]
    adminId?: string; // 管理员编号 [最大位数:20]
    adminName?: string; // 管理员名称 [最大位数:100]
    points: number; // 积分数负数表示扣除 [最大位数:11]
    addTime: string; // 添加时间
    plDesc: string; // 操作描述 [最大位数:100]
    plStage: string; // 操作阶段 [最大位数:50]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type MemberPoints = {
    memberId: string; // 会员编号 [最大位数:20]
    memberName: string; // 会员名称 [最大位数:100]
    memberPoints: number; // 积分数负数表示扣除 [最大位数:11]
  };

  export type Seo = {
    seoId: number; // 主键 [最大位数:11]
    title: string; // 标题 [最大位数:255]
    keywords: string; // 关键词 [最大位数:255]
    description: string; // 描述
    seoType: string; // 类型 [最大位数:20]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 邮件模板
  export type MessageTemplate = {
    templateId: number; // 主键 [最大位数:11]
    templateCode: string; // 模板code [最大位数:100]
    templateName: string; // 模板名称 [最大位数:100]
    templateType: string; // 模板type: store member others [最大位数:100]
    messageEnable: boolean; // 站内信默认开关，0关，1开 [最大位数:1]
    messageForced: boolean; // 站内信强制接收，0否，1是 [最大位数:1]
    messageContent: string; // 站内信内容 [最大位数:255]
    shortEnable: boolean; // 短信默认开关，0关，1开 [最大位数:1]
    shortForced: boolean; // 短信强制接收，0否，1是 [最大位数:1]
    shortContent: string; // 短信内容 [最大位数:255]
    mailEnable: boolean; // 邮件默认开，0关，1开 [最大位数:1]
    mailForced: boolean; // 邮件强制接收，0否，1是 [最大位数:1]
    mailSubject: string; // 邮件标题 [最大位数:255]
    mailContent?: string; // 邮件内容
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 支付方式
  export type PaymentMethod = {
    paymentMethodId: number; // 支付索引id [最大位数:11]
    paymentMethodCode: string; // 支付代码名称 [最大位数:20]
    name: string; // 支付名称 [最大位数:20]
    config?: string; // 支付接口配置信息
    state: boolean; // 接口状态0禁用1启用 [最大位数:1]
    siteUrl?: string; // site url [最大位数:80]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
    detail: any;
  };

  // 快递公司
  export type Express = {
    expressId: number; // 索引ID [最大位数:11]
    expressName: string; // 公司名称 [最大位数:50]
    defaultCode: string; // 公司编号：快递100专用 [最大位数:150]
    otherCode?: string; // 其他公司编号，预留 [最大位数:150]
    state: boolean; // 状态 1:可用 0:不可用 [最大位数:1]
    famous: boolean; // 是否常用 1常用0不常用 [最大位数:1]
    url?: string; // 公司网址 [最大位数:100]
    logo?: string; // logo [最大位数:200]
    listOrder: number; // 编号：0-255,越大排在前面 [最大位数:10]
    pickUpSite?: boolean; // 是否支持服务站配送0否1是 [最大位数:1]
    expressType?: string; // 公司类型 [最大位数:150]
    expressTags?: string; // 公司标签，用逗号分隔 [最大位数:250]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 省 市 县
  export type Area = {
    areaId: number; // 索引ID [最大位数:11]
    code: string; // 地区code [最大位数:30]
    name: string; // 地区名称 [最大位数:60]
    province: string; // 省级(省/直辖市/特别行政区) [最大位数:10]
    city: string; // 地级(城市) [最大位数:10]
    area: string; // 县级(区县) [最大位数:10]
    town: string; // 乡级(乡镇/街) [最大位数:20]
    isShow?: boolean; // 是否有效 [最大位数:1]
    initial?: string; // 字母 [最大位数:2]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 省 市 县
  export type Town = {
    townId: number; // 索引ID [最大位数:11]
    code: string; // 地区code [最大位数:30]
    name: string; // 地区名称 [最大位数:60]
    province: string; // 省级(省/直辖市/特别行政区) [最大位数:10]
    city: string; // 地级(城市) [最大位数:10]
    area: string; // 县级(区县) [最大位数:10]
    town: string; // 乡级(乡镇/街) [最大位数:20]
    isShow?: boolean; // 是否有效 [最大位数:1]
    initial?: string; // 字母 [最大位数:2]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type Brand = {
    brandId: number; // 品牌id [最大位数:11]
    brandName: string; // 品牌名称 [最大位数:100]
    brandInitial?: string; // 品牌首字母 [最大位数:1]
    brandRemarks?: string; // 品牌remarks [最大位数:500]
    brandLogo?: string; // 图片 [最大位数:100]
    brandSort?: number; // 排序 [最大位数:11]
    brandRecommend?: boolean; // 推荐，0为否，1为是，默认为0 [最大位数:1]
    storeId: number; // 店铺ID [最大位数:11]
    brandApply: number; // 品牌申请，0为申请中，1为通过，默认为1，申请功能是会员使用，系统后台默认为1 [最大位数:11]
    showType: number; // 品牌展示类型 0表示图片 1表示文字  [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type GoodsClass = {
    classId: number; // 索引ID [最大位数:11]
    className: string; // 分类名称 [最大位数:100]
    classSort: number; // 排序 [最大位数:11]
    virtualEnable: boolean; // 是否允许发布虚拟商品，1是，0否 [最大位数:1]
    title: string; // 名称 no used [最大位数:200]
    keywords: string; // 关键词 [最大位数:255]
    description: string; // 描述 [最大位数:255]
    classType: string; // 分类类型 no used [最大位数:255]
    typeId: number; // 类型id--will delete [最大位数:10]
    typeName: string; // 类型名称--will delete [最大位数:100]
    parentId: number; // 父ID [最大位数:11]
    commissionRate: number; // 佣金比例0-100 example:1.5 0.1
    pic?: string; // pc端分类图标 [最大位数:500]
    icon?: string; // 手机端分类图标 [最大位数:1000]
    promotionsUrl?: string; // 手机端推广广告地址 [最大位数:1000]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type GoodsClassTree = {
    classId: number; // 索引ID [最大位数:11]
    className: string; // 分类名称 [最大位数:100]
    classSort: number; // 排序 [最大位数:11]
    key: number;
    title: string;
    children: GoodsClassTree[] | undefined | null;
  };

  export type GoodsClassCollect = {
    collectId: number; // 显示类目ID [最大位数:11]
    collectName: string; // 显示类目名 [最大位数:100]
    isUse: boolean; // 是否启用：0不启用 1启用 [最大位数:1]
    slideSort: number; // 排序 [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type GoodsClassCollectItem = {
    collectItemId: number; // 显示类目项自增标识编号 [最大位数:11]
    collectId: number; // 显示类目id [最大位数:11]
    title: string; // 显示名称 [最大位数:255]
    classId: number; // 分类ID [最大位数:11]
    slideSort: number; // 排序 [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type GoodsClassCollectParams = {
    collect: API.GoodsClassCollect;
    collectItems: API.GoodsClassCollectItem[];
  };

  export type Spec = {
    specId: number; // 规格id [最大位数:11]
    specName: string; // 规格名称 [最大位数:100]
    specSort: number; // 排序 [最大位数:11]
    classId?: number; // 所属分类id [最大位数:11]
    className?: string; // 所属分类名称 [最大位数:100]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type SpecValue = {
    specValueId: number; // 规格值id [最大位数:11]
    specValueName: string; // 规格值名称 [最大位数:100]
    specValue?: string; // 规格颜色 [最大位数:100]
    specValueSort: number; // 排序 [最大位数:11]
    specId: number; // 所属规格id [最大位数:11]
    classId: number; // 分类id [最大位数:11]
    storeId: number; // 店铺id [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type GoodsType = {
    typeId: number; // 类型id [最大位数:11]
    typeName: string; // 类型名称 [最大位数:100]
    typeSort: number; // 排序 [最大位数:11]
    classId?: number; // 所属分类id--will delete [最大位数:11]
    className?: string; // 所属分类名称--will delete [最大位数:100]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type Attribute = {
    attributeId: number; // 属性id [最大位数:11]
    attributeName: string; // 属性名称 [最大位数:100]
    attributeValue: string; // 属性值列
    attributeShow: boolean; // 是否显示。0为不显示、1为显示 [最大位数:1]
    attributeSort: number; // 排序 [最大位数:11]
    typeId: number; // 所属类型id [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  export type AttributeValue = {
    attributeValueId: number; // 属性值id [最大位数:11]
    attributeValue: string; // 属性值 [最大位数:100]
    attributeValueSort: number; // 属性值排序 [最大位数:11]
    attributeId: number; // 所属属性id [最大位数:11]
    typeId: number; // 类型id [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 后台管理员查询用到的商品公共对象
  export type GoodsCommonForList = {
    goodsCommonId: number; // 商品公共表id [最大位数:11]
    goodsName: string; // 商品名称 [最大位数:150]
    storeName: string; // 店铺名称 [最大位数:50]
    brandName: string; // 品牌名称 [最大位数:100]
    gcName?: string; // 商品分类 [最大位数:200]
    goodsImage: string; // 商品主图 [最大位数:200]
    goodsState: number; // 商品状态 0下架，1正常，10违规（禁售） [最大位数:11]
    goodsVerify?: number; // 商品审核 1通过，0未通过，10审核中 [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
    goodsStateRemark?: string; // 违规原因 [最大位数:255]
    goodsVerifyRemark?: string; // 审核失败原因 [最大位数:255]
  };

  // 后台管理员查询用到的商品公共对象
  export type GoodsForList = {
    goodsId: number; // 商品id(SKU) [最大位数:11]
    goodsCommonId: number; // 商品公共表id [最大位数:11]
    goodsName: string; // 商品名称 [最大位数:150]
    goodsImage: string; // 商品主图 [最大位数:200]
    goodsPrice?: number; // 商品价格 [最大位数:10,2]
    goodsStorage?: number; // 商品库存 [最大位数:11]
    colorId?: number; // 颜色规格id [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 相册
  export type Album = {
    albumId: number; // 相册id [最大位数:11]
    albumName: string; // 相册名称 [最大位数:100]
    storeId: number; // 所属店铺id [最大位数:11]
    storeName?: string; // 店铺名称 [最大位数:50]
    albumDes: string; // 相册描述 [最大位数:255]
    albumSort: number; // 排序 [最大位数:11]
    albumCover: string; // 相册封面 [最大位数:255]
    uploadTime?: string; // 图片上传时间
    isDefault: boolean; // 是否为默认相册,1代表默认 [最大位数:1]
    picNum: number; // 包含图片数量 [最大位数:11]
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // 相册图片
  export type AlbumPic = {
    albumPicId: number; // 相册图片表id [最大位数:11]
    albumPicName: string; // 图片名称 [最大位数:100]
    albumPicTag: string; // 图片标签 [最大位数:255]
    albumPicCover: string; // 图片路径 [最大位数:255]
    albumPicSize: number; // 图片大小 [最大位数:11]
    albumPicSpec: string; // 图片规格 [最大位数:100]
    albumId: number; // 相册id [最大位数:11]
    storeId: number; // 所属店铺id [最大位数:11]
    uploadTime: string; // 图片上传时间
    gmtCreate?: string; // 记录创建时间
    gmtModified?: string; // 记录修改时间
  };

  // -----------------------------------------
  export type CurrentUser = {
    user: Admin;
    authorities: string;
    shortcut: string;
  };

  export type LoginStateType = {
    status?: 'ok' | 'error';
    type?: string;
  };

  export type NoticeIconData = {
    id: string;
    key: string;
    avatar: string;
    title: string;
    datetime: string;
    type: string;
    read?: boolean;
    description: string;
    clickClose?: boolean;
    extra: any;
    status: string;
  };
}
