import './BasicLayout.less';

import React from 'react';

// 直接引用ProLayout的接口，今后紧跟Pro的规范
import type { BasicLayoutProps } from '@ant-design/pro-layout';

// 引入ant-design组件
import { Layout } from 'antd';

import MyFooter from '@/components/Footer';

const { Footer, Content } = Layout;

/**
 * 🌃 Powerful and easy to use beautiful layout
 * 🏄‍ Support multiple topics and layout types
 * @param props
 */
const BasicLayout: React.FC<BasicLayoutProps> = (props) => {
  const { children } = props;

  return (
    <div style={{ display: 'flex', flexDirection: 'row' }}>
      <div
        style={{ backgroundColor: 'red', width: 64 + 130, display: 'flex', flexDirection: 'row' }}
      >
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ background: 'pink', width: 64 }}>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br /> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br /> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br /> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br /> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br /> <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            main
          </div>
          <div style={{ background: 'blue', width: 130 }}>sub</div>
        </div>
      </div>
      <Layout>
        <Content>{children}</Content>
        <Footer>
          <MyFooter />
        </Footer>
      </Layout>
    </div>
  );
};

export default BasicLayout;
