import React from 'react';
import './index.less';
import classNames from 'classnames';
import { createFromIconfontCN } from '@ant-design/icons';

import getIcon from '../getIconReactNode';
import { history } from 'umi';

import { useIntl } from 'umi';

import type { MenuDataItem } from '@ant-design/pro-layout/lib/typings';

type menuBoxProps = {
  text?: string;
  iconReactNode?: React.ReactNode;
  selected?: boolean;
  menu: MenuDataItem;
};

/**
 * 菜单组件
 * @param props
 */
const MenuBox = (props: menuBoxProps) => {
  return (
    <a
      className={classNames({
        menuBoxLg: true,
        selected: props.selected,
        noSelected: !props.selected,
      })}
      onClick={() => {
        if (props.menu.redirect) {
          history.push(props.menu.redirect);
        } else {
          // 如果有子菜单，那么就跳转到子菜单中
          const firstChildrenMenu = props.menu.children ? props.menu.children[0] : undefined;
          if (firstChildrenMenu) {
            history.push(firstChildrenMenu.path as string);
          }
        }
      }}
    >
      <div className={classNames('menuIcon')}>{props.iconReactNode}</div>
      <div className={classNames('menuText')}>{props.text}</div>
    </a>
  );
};

/**
 * Logo组件
 * @param props
 */
const Logo = (props: { src: string }) => {
  return (
    <div className={classNames('logoBox')}>
      <a>
        <img src={props.src} alt="logo" />
      </a>
    </div>
  );
};

/**
 * 主要侧边栏属性
 * @param props
 */

type mainSiderProps = {
  matchMenuKeys?: string[];
  menuData?: MenuDataItem[];
  onMenuHeaderClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
  hide?: boolean;
  iconScriptUrl?: string;
};

/**
 * 主要侧边栏组件
 * @param props
 * @returns
 */
const MainSider: React.FC<mainSiderProps> = (props) => {
  const { menuData, matchMenuKeys } = props;
  // 如果当前选中的matchMenuKeys不为空，那么就返回数组的第一个。 matchMenuKeys的数据例如：["/admin/welcome","/admin/welcome/home"]
  const selectMenuKey = matchMenuKeys ? matchMenuKeys?.[0] : '';
  const { formatMessage } = useIntl();
  const IconFont = createFromIconfontCN({
    scriptUrl: props.iconScriptUrl,
  });

  return (
    <div className={classNames('mainSider')}>
      <Logo src="https://gw.alipayobjects.com/zos/antfincdn/PmY%24TNNDBI/logo.svg" />

      {menuData?.map((menu) => {
        const text = formatMessage({
          id: menu.locale || '',
          defaultMessage: menu.name,
        });

        const iconReactNode = getIcon(menu.icon, IconFont);
        return (
          <MenuBox
            text={text}
            key={menu.key || menu.path}
            menu={menu}
            iconReactNode={iconReactNode}
            selected={menu.path === selectMenuKey}
          />
        );
      })}
    </div>
  );
};

export default MainSider;
