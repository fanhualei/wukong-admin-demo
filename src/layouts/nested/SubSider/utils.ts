/**
 * 这个工具类中封装了如何得到一个快捷菜单的函数
 * 以前这段代码放在SubSlide代码中，后来发现那个文件中的代码太多了，所以就挪了出来。
 * 这里面有一个主函数用来得到子菜单。
 */

import store from 'store';

import type { MenuDataItem } from '@ant-design/pro-layout/lib/typings';

// ---------------------------------讲快捷键保存到本地缓存中-----------------------------------------------

export type ShortcutJsonProps = {
  title: string;
  href: string;
};

const SHORTCUT_STORE_NAME: string = 'shortcutMenu';

/**
 * 得到本地保存的快捷菜单的信息
 * @returns
 */
export function getShortcutFromStore(): ShortcutJsonProps[] | undefined {
  const infoObj: ShortcutJsonProps[] = store.get(SHORTCUT_STORE_NAME);
  if (infoObj === undefined) {
    return [];
  }
  return infoObj;
}

/**
 * 返回一个字符串，例如： "/admin/goods/class,/admin/goods/type" : 这里注意没有admin
 */
export function getShortcutForTree(): string[] {
  const infoObj: ShortcutJsonProps[] = store.get(SHORTCUT_STORE_NAME);
  if (infoObj === undefined) {
    return [];
  }
  const ren: string[] = [];
  for (let i = 0; i < infoObj.length; i += 1) {
    if (infoObj[i].href.trim().length > 0) {
      const temp = infoObj[i].href.substring('/admin'.length);
      ren.push(temp);
    }
  }
  return ren;
}

/**
 * 设置本地保存的快捷菜单信息
 * @param shortcutMenu
 */
export function saveShortcutToStore(shortcutMenu: ShortcutJsonProps[]) {
  store.set(SHORTCUT_STORE_NAME, shortcutMenu);
}

/**
 * 清除掉本地缓存中的快捷菜单，如果什么都没有选择的话
 */
export function clearnShortcutToStore() {
  store.set(SHORTCUT_STORE_NAME, '');
}

// ---------------------------------菜单相关-----------------------------------------------

/**
 * 得到一个Menu菜单
 * @param key
 * @param allMenuData
 * @returns
 */
const getOneMenu = (key: string, allMenuData: MenuDataItem[]) => {
  const parentKey = key.substring(0, key.lastIndexOf('/'));
  // 先找到一级菜单
  for (let i = 0; i < allMenuData.length; i += 1) {
    const parentMenu = allMenuData[i];
    if (parentMenu.path === parentKey && parentMenu.children) {
      // 在一级菜单中找相应的菜单
      for (let j = 0; j < parentMenu.children.length; j += 1) {
        const subMenu = parentMenu.children[j];
        if (subMenu.path === key) {
          return subMenu;
        }
      }
    }
  }
  return undefined;
};

/**
 * 用来得到快捷菜单
 * @param allMenuData
 * @returns
 */
export function getShortcutMenus(allMenuData: MenuDataItem[]) {
  // 从本地缓存得到快捷菜单
  const array = getShortcutFromStore() || [];
  const ren: MenuDataItem[] = [];

  array.forEach((item) => {
    const shortcutMenu = getOneMenu(item.href, allMenuData);
    if (shortcutMenu) {
      ren.push(shortcutMenu);
    }
  });

  return ren;
}
