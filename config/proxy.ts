/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 * The agent cannot take effect in the production environment
 * so there is no configuration of the production environment
 * For details, please see
 * https://pro.ant.design/docs/deploy
 */

const apiServerLocal = 'http://192.168.31.213:8080';
const apiServer = 'http://adminapi.redhtc.com';

export default {
  dev: {
    '/api': {
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/oauth': {
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/imgs': {
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  local: {
    '/api': {
      target: apiServerLocal,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/oauth': {
      target: apiServerLocal,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/imgs': {
      target: apiServerLocal,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/dev': {
      target: apiServerLocal,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },

  test: {
    '/api': {
      // target: 'https://preview.pro.ant.design',
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/oauth': {
      // target: 'https://preview.pro.ant.design',
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/imgs': {
      // target: 'https://preview.pro.ant.design',
      target: apiServer,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  pre: {
    '/api/': {
      target: 'your pre url',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
};
